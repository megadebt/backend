package auth

import (
	"context"
	"fmt"

	"megadebt/model"

	firebase "firebase.google.com/go"
	"golang.org/x/xerrors"
)

type FirebaseAuthentificator struct {
	App *firebase.App
}

func (fa *FirebaseAuthentificator) Init(ctx context.Context, _ string) {
	// firebase app init
	// panics, if GOOGLE_APPLICATION_CREDENTIALS is not set
	app, err := firebase.NewApp(ctx, nil)
	if err != nil {
		panic(fmt.Sprintf("failed to create new firebase app: Reason: %v", err))
	}
	if app == nil {
		panic(fmt.Sprintf("failed to init new firebase app: app is nil after creation"))
	}
	fa.App = app
}

func (fa *FirebaseAuthentificator) UserAuth(ctx context.Context, token string) (model.User, error) {
	client, err := fa.App.Auth(ctx)
	if err != nil {
		return model.User{}, xerrors.Errorf("error getting Auth client: %v", err)
	}

	firebaseToken, err := client.VerifyIDToken(ctx, token)
	if err != nil {
		return model.User{}, xerrors.Errorf("error verifying ID token: %v", err)
	}

	firebaseUser, err := client.GetUser(ctx, firebaseToken.UID)
	if err != nil {
		return model.User{}, xerrors.Errorf("failed to get user %s by token: %v", firebaseToken.UID, err)
	}

	user := FirebaseUserToModelUser(firebaseUser)
	return user, nil
}

func (fa *FirebaseAuthentificator) VerifyToken(ctx context.Context, token string) (string, error) {
	client, err := fa.App.Auth(ctx)
	if err != nil {
		return "", xerrors.Errorf("error getting Auth client: %v", err)
	}

	firebaseToken, err := client.VerifyIDToken(ctx, token)
	if err != nil {
		return "", xerrors.Errorf("error verifying ID token: %v", err)
	}

	return firebaseToken.UID, nil
}
