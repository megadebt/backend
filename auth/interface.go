package auth

import (
	"context"

	"megadebt/model"
)

type IAuthentificator interface {
	UserAuth(ctx context.Context, token string) (model.User, error)
	VerifyToken(ctx context.Context, token string) (string, error)
}

type Mock struct{}

func (m *Mock) UserAuth(ctx context.Context, token string) (model.User, error) {
	return model.User{
		ID:          "7NG4RxAvFZScIaTAKtVK9mJHDYh2",
		Email:       "ayuka@gmail.com",
		Login:       "ayuka",
		Rating:      5.0,
		PhotoURL:    "https://pisos.com/1.png",
		DisplayName: "Povar Petrovich",
	}, nil
}

func (m *Mock) VerifyToken(ctx context.Context, token string) (string, error) {
	return "7NG4RxAvFZScIaTAKtVK9mJHDYh2", nil
}
