package auth

import (
	"context"
	"net/http"

	"megadebt/library/logger"
	"megadebt/model"

	"golang.org/x/xerrors"
)

// Type of key to use when setting user in context
type ctxKeyUser int

// Unique key that holds the user in request context
const userKey ctxKeyUser = 0

func Authentificate(logger *logger.ContextLogger, authentificator IAuthentificator, authHeader string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			tokenCandidate := r.Header.Get(authHeader)
			user, err := authentificator.UserAuth(r.Context(), tokenCandidate)
			if err != nil {
				logger.WithContext(r.Context()).Warnf("failed to check user token: %v", err)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			ctx := ContextWithUser(r.Context(), user)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func ContextWithUser(ctx context.Context, user model.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

func GetUserFromContext(ctx context.Context) (model.User, error) {
	user := ctx.Value(userKey)
	if user == nil {
		return model.User{}, xerrors.New("failed to get user from context: unauthorized")
	}
	if castedUser, ok := user.(model.User); !ok {
		return model.User{}, xerrors.New("failed to cast user from context to auth user")
	} else {
		return castedUser, nil
	}
}
