package auth

import (
	"megadebt/model"

	firebaseauth "firebase.google.com/go/auth"
)

func FirebaseUserToModelUser(ur *firebaseauth.UserRecord) model.User {
	var photoURL string
	if len(ur.PhotoURL) > 0 {
		photoURL = ur.PhotoURL
	}
	if len(photoURL) == 0 && len(ur.ProviderUserInfo) > 0 {
		for _, providerUI := range ur.ProviderUserInfo {
			if len(providerUI.PhotoURL) > 0 {
				photoURL = providerUI.PhotoURL
				break
			}
		}
	}
	return model.User{
		ID:          ur.UID,
		DisplayName: ur.DisplayName,
		Email:       ur.Email,
		PhotoURL:    photoURL,
	}
}
