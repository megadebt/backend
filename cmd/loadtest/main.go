package main

import (
	"megadebt/library/logger"
	"megadebt/loadtest"
)

func ElClassico(runner loadtest.IRunner) {
	scenario := loadtest.NewScenario(loadtest.ReceiptCreatingScenarioType, runner.Host())
	runner.RunScenario(scenario, 10)
}

func main() {
	myLogger := logger.InitLogging()
	runner := &loadtest.Runner{}
	runner.Init(10, myLogger, "https://megadebt-2vwg3zd3bq-uc.a.run.app")
	ElClassico(runner)
}
