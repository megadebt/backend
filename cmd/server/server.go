package main

import (
	"net/http"

	"megadebt/server"
)

func main() {
	server := megadebt.Server{}
	server.Init()
	server.Logger.Info("Starting Megadebt...")
	http.ListenAndServe(":8080", server.Router)
}
