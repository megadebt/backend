package db

import (
	"context"
	"megadebt/library/logger"

	"github.com/jackc/pgx"
)

type IClient interface {
	Init(ctx context.Context, logger *logger.ContextLogger)
	Connect(ctx context.Context) (*pgx.Conn, error)
	Close(ctx context.Context, connection *pgx.Conn)
	Query(ctx context.Context, query string, scanner IScanner) error
	Exec(ctx context.Context, query string) error
}
