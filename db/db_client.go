package db

import (
	"context"
	"time"

	"megadebt/library/logger"
	"megadebt/loadtest"

	"github.com/jackc/pgx"
	"golang.org/x/xerrors"
)

type Client struct {
	ConnConfig *pgx.ConnConfig
	Logger     *logger.ContextLogger
}

func (db *Client) Close(ctx context.Context, connection *pgx.Conn) {
	defer loadtest.MeasureTiming(time.Now(), "Connection closing")
	if connection == nil {
		db.Logger.Error("Connection is empty. Nothing to close")
	} else {
		err := connection.Close(ctx)
		if err != nil {
			db.Logger.Errorf("Error occurred during connection closing: %v", err)
		} else {
			db.Logger.Info("Connection successfully disposed")
		}
	}
}

func (db *Client) Init(ctx context.Context, logger *logger.ContextLogger) {
	// var connConfig, _ = pgx.ParseConfig("postgresql://postgres:228pasany@35.228.250.68:5432/postgres")
	var connConfig, _ = pgx.ParseConfig("postgres://postgres:228pasany@/postgres?host=/cloudsql/steadfast-canto-277412:europe-north1:megadebtturbo")
	db.Logger = logger
	db.Logger.Info("PgConfig initialized")
	db.ConnConfig = connConfig
}

func (db *Client) Connect(ctx context.Context) (*pgx.Conn, error) {
	var timer loadtest.TimeBenchmark
	timer.Start()
	conn, err := pgx.ConnectConfig(ctx, db.ConnConfig)
	timer.ElapsedFor("Fetching ConnectConfig")
	if err != nil {
		return nil, xerrors.Errorf("Error occurred during connection initialization %v", err)
	}
	if conn == nil {
		db.Logger.Error("Connection initialization failed. Connection is null or empty")
		return nil, xerrors.Errorf("Connection initialization failed. Connection is null or empty")
	}
	db.Logger.Info("Connection successfully established.")
	return conn, nil
}

func (db *Client) Query(ctx context.Context, query string, scanner IScanner) error {
	conn, err := db.Connect(ctx)

	defer db.Close(ctx, conn)
	if err != nil {
		return xerrors.Errorf("DB connection failed: `%w`", err)
	}
	var timer loadtest.TimeBenchmark
	timer.Start()
	result, err := conn.Query(ctx, query)
	if err != nil {
		return xerrors.Errorf("Error occurred during query execution: `%w`", err)
	}
	timer.ElapsedFor("Pgx Querying")
	for result.Next() {
		err = scanner.Scan(ctx, result)
		if err != nil {
			return xerrors.Errorf("Scanning row `%v` failed: %w", result, err)
		}
	}
	return nil
}

func (db *Client) Exec(ctx context.Context, query string) error {
	conn, err := db.Connect(ctx)

	defer db.Close(ctx, conn)
	if err != nil {
		return xerrors.Errorf("DB connection failed: `%w`", err)
	}
	transaction, err := conn.Begin(ctx)
	if err != nil {
		return xerrors.Errorf("Error occurred during transaction beginning")
	}
	commandTag, err := transaction.Exec(ctx, query)
	if err != nil {
		return xerrors.Errorf("Error occurred during query execution. Error message: %w", err)
	}
	err = transaction.Commit(ctx)
	if err != nil {
		err := transaction.Rollback(ctx)
		if err != nil {
			return xerrors.Errorf("Fatal error occurred during transaction rollback after committing error (%w): %w", err)
		}
		return xerrors.Errorf("Fatal error occurred during transaction committing: %w. Transaction undone.", err)
	}
	if commandTag.RowsAffected() == 0 {
		db.Logger.Warn("[WARN] No rows affected")
	}
	db.Logger.Info("Query executed successfully!")
	return nil
}
