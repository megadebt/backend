package db

import (
	"context"
	"github.com/jackc/pgx"
	"megadebt/library/logger"
	"megadebt/loadtest"
	"time"
)

type ClientWithMetrics struct {
	Logger *logger.ContextLogger
	Client *Client
}

func (database *ClientWithMetrics) Close(ctx context.Context, connection *pgx.Conn) {
	defer loadtest.MeasureTiming(time.Now(), "Closing Connection")
	database.Client.Close(ctx, connection)
}

func (database *ClientWithMetrics) Init(ctx context.Context, logger *logger.ContextLogger) {
	database.Client = &Client{}
	database.Logger = logger
	database.Client.Init(ctx, logger)
}

func (database *ClientWithMetrics) Connect(ctx context.Context) (*pgx.Conn, error) {
	defer loadtest.MeasureTiming(time.Now(), "Database Connection")
	return database.Client.Connect(ctx)
}

func (database *ClientWithMetrics) Query(ctx context.Context, query string, scanner IScanner) error {
	defer loadtest.MeasureTiming(time.Now(), "Query Execution")
	return database.Client.Query(ctx, query, scanner)
}

func (database *ClientWithMetrics) Exec(ctx context.Context, query string) error {
	defer loadtest.MeasureTiming(time.Now(), "ExecQuery Execution")
	return database.Client.Exec(ctx, query)
}
