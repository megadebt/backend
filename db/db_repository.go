package db

import (
	"context"
	"fmt"
	"strings"

	"megadebt/library/logger"

	"megadebt/model"

	"github.com/ahmetb/go-linq"
	"github.com/gofrs/uuid"
	"golang.org/x/xerrors"
)

type Repository struct {
	Client IClient
	Logger *logger.ContextLogger
}

func (r *Repository) Init(logger *logger.ContextLogger) {
	r.Client = &Client{}
	r.Logger = logger
	r.Client.Init(context.Background(), logger)
}

func (r *Repository) SelectUserReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	receipts, err := r.selectAllUserReceipts(ctx, user)

	if err != nil {
		return nil, xerrors.Errorf("Error fetching active user receipts. Error: %w", err)
	}

	var result []model.ReceiptShort
	linq.From(receipts).WhereT(func(receipt model.ReceiptShort) bool {
		return receipt.State() != model.ReceiptStateClosed
	}).ToSlice(&result)

	return result, nil
}

func (r *Repository) SelectUserHistoryReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	receipts, err := r.selectAllUserReceipts(ctx, user)

	if err != nil {
		return nil, xerrors.Errorf("Error fetching historical user receipts. Error: %w", err)
	}

	var result []model.ReceiptShort
	linq.From(receipts).WhereT(func(receipt model.ReceiptShort) bool {
		return receipt.State() == model.ReceiptStateClosed
	}).ToSlice(&result)

	return result, nil
}

func (r *Repository) SelectUserReceiptByID(ctx context.Context, user model.User, receiptID string) (model.Receipt, error) {
	var receiptScanner ReceiptScanner
	var itemScanner ItemScanner
	var userScanner UserScanner

	var query = fmt.Sprintf(
		`SELECT
					r."Id",
					r."Date",
					r."Name",
					o."Id",
					o."Login",
					o."Email",
					o."Rating",
					o."PhotoUrl",
					o."DisplayName",
					r."Description",
					d."Id",
					d."State",
					d."PaidDate",
					d."Amount",
					d."ReceiptId",
					b."Id",
					b."Login",
					b."Email",
					b."Rating",
					b."PhotoUrl",
					b."DisplayName",
					deb."Id",
					deb."Login",
					deb."Email",
					deb."Rating",
					deb."PhotoUrl",
					deb."DisplayName"
				FROM public."Receipt" r
				LEFT JOIN public."User" o ON o."Id" = r."OwnerId"
				LEFT JOIN public."Debt" d ON d."ReceiptId" = r."Id"
				LEFT JOIN public."User" b ON b."Id" = d."BorrowerId"
				LEFT JOIN public."User" deb ON deb."Id" = d."DebtorId"
				WHERE r."Id" = '%s'`, receiptID)

	err := r.Client.Query(ctx, query, &receiptScanner)

	if err != nil {
		return model.Receipt{}, xerrors.Errorf("Failed to query through Receipts for user `%s`: %w", user.ID, err)
	}

	var receipts []model.Receipt
	linq.From(receiptScanner.Scanned).SelectT(func(value linq.KeyValue) model.Receipt {
		return value.Value.(model.Receipt)
	}).ToSlice(&receipts)

	if len(receipts) == 0 {
		return model.Receipt{}, xerrors.Errorf("Receipts for user `%s` with receiptID `%s` not found", user.ID, receiptID)
	}

	if len(receipts) > 1 {
		return model.Receipt{}, xerrors.Errorf("Collision occurred! More than one receipt found for user `%s` with receiptID `%s`", user.ID, receiptID)
	}

	var itemQuery = fmt.Sprintf(
		`SELECT
					i."Id",
					i."Name",
					i."Price",
					i."Quantity",
					p."Id",
					p."Login",
					p."Email",
					p."Rating",
					p."PhotoUrl",
					p."DisplayName"
				FROM public."Item" i
				LEFT JOIN public."ItemToUsers" itu ON itu."ItemId" = i."Id"
				LEFT JOIN public."User" p ON p."Id" = itu."UserId"
				WHERE i."ReceiptId" = '%s'`, receipts[0].ID)

	err = r.Client.Query(ctx, itemQuery, &itemScanner)
	if err != nil {
		return model.Receipt{}, xerrors.Errorf("Failed to query through receipts by receiptID `%s`: %w", receiptID, err)
	}

	var items []model.Item
	linq.From(itemScanner.Scanned).SelectT(func(value linq.KeyValue) model.Item {
		return value.Value.(model.Item)
	}).ToSlice(&items)

	receipts[0].Items = items

	var recipientsQuery = fmt.Sprintf(
		`SELECT
					u."Id",
					u."Login",
					u."Email",
					u."Rating",
					u."PhotoUrl",
					u."DisplayName"
				FROM public."ReceiptParticipants" p
				INNER JOIN public."User" u ON u."Id" = p."UserId"
				WHERE p."ReceiptId" = '%s'
				`, receipts[0].ID)

	err = r.Client.Query(ctx, recipientsQuery, &userScanner)
	if err != nil {
		return model.Receipt{}, xerrors.Errorf("Failed to query through ReceiptParticipants by receiptID `%s`: %w", receiptID, err)
	}

	receipts[0].Participants = userScanner.Scanned

	return receipts[0], nil
}

func (r *Repository) SelectUserDebtByID(ctx context.Context, user model.User, debtID string) (model.Debt, error) {
	var debtScanner DebtScanner

	var query = fmt.Sprintf(
		`SELECT
					d."Id",
					d."State",
					d."Amount",
					b."Id",
					b."Login",
					b."Email",
					b."Rating",
					b."PhotoUrl",
					b."DisplayName",
					deb."Id",
					deb."Login",
					deb."Email",
					deb."Rating",
					deb."PhotoUrl",
					deb."DisplayName",
					d."PaidDate",
					d."ReceiptId"
				FROM public."Debt" d
				LEFT JOIN public."User" b ON b."Id" = d."BorrowerId"
				LEFT JOIN public."User" deb ON deb."Id" = d."DebtorId"
				WHERE (d."DebtorId" = '%s' OR d."BorrowerId" = '%s') AND d."Id" = '%s'`, user.ID, user.ID, debtID)

	err := r.Client.Query(ctx, query, &debtScanner)

	if err != nil {
		return model.Debt{}, xerrors.Errorf("Failed to query through Debts for user `%s` with debtID `%s`: %w", user.ID, debtID, err)
	}

	if len(debtScanner.Scanned) == 0 {
		return model.Debt{}, xerrors.Errorf("Debts for user `%s` with debtID `%s` not found", user.ID, debtID)
	}

	if len(debtScanner.Scanned) > 1 {
		return model.Debt{}, xerrors.Errorf("Collision occurred! More than one debt found for user `%s` with debtID `%s`", user.ID, debtID)
	}

	return debtScanner.Scanned[0], nil
}

func (r *Repository) CreateUserReceipt(ctx context.Context, user model.User, receipt model.Receipt) (string, error) {

	receipt, err := getReceiptWithGeneratedIds(receipt)

	if err != nil {
		return "", xerrors.Errorf("Failed to generate ids for receipt or nested items. Error: %w", err)
	}

	receipt.Owner = user

	debts, err := receipt.GenerateDebts()

	if err != nil {
		return "", xerrors.Errorf("Failed to generate debts for receipt. Error: %w", err)
	}

	var addReceiptQuery = createAddReceiptQuery(receipt, user)
	var addItemsQuery = createAddItemsQuery(receipt)
	var addItemUsersQuery = createItemUsersQuery(receipt)
	var addDebtQuery = createAddDebtQuery(receipt, debts, user)
	var addParticipantQuery = createAddParticipantQuery(receipt)
	var execQuery = fmt.Sprintf(
		`%s; --AddReceiptQuery
%s; --AddItemsQuery
%s; --AddItemUsersQuery
%s; --AddDebtQuery
%s; --AddParticipantQuery`,
		addReceiptQuery,
		addItemsQuery,
		addItemUsersQuery,
		addDebtQuery,
		addParticipantQuery)

	err = r.Client.Exec(ctx, execQuery)

	if err != nil {
		return "", xerrors.Errorf("Failed to execute INSERT query for Receipt.\nParams:\n"+
			"UserID: `%s`\nErrorMessage:\n%w", user.ID, err)
	}

	return receipt.ID, nil
}

func (r *Repository) StoreUserDebtState(ctx context.Context, user model.User, debtID string, debtState model.DebtState) error {
	var execQuery = fmt.Sprintf(`
		UPDATE public."Debt" 
		SET "State" = '%s' 
		WHERE "Id" = '%s' AND ("DebtorId" = '%s' OR "BorrowerId" = '%s')`,
		string(debtState),
		debtID,
		user.ID,
		user.ID)

	err := r.Client.Exec(ctx, execQuery)

	if err != nil {
		return xerrors.Errorf("Failed to update DebtState. Params: \nDebtID: %s; UserID: %s; \nError: %w", debtID, user.ID, err)
	}

	return nil
}

func (r *Repository) CloseUserReceipt(ctx context.Context, user model.User, receiptID string) error {

	var execQuery = fmt.Sprintf(`
		UPDATE public."Debt"
		SET "State" = '%s'
		WHERE "BorrowerId" = '%s' AND "ReceiptId" = '%s'`,
		string(model.DebtStateClosed),
		user.ID,
		receiptID)

	err := r.Client.Exec(ctx, execQuery)

	if err != nil {
		return xerrors.Errorf("Failed to close user receipt. Params:\nReceiptID: %s; UserID: %s; \nError: %w",
			receiptID,
			user.ID,
			err)
	}

	return nil
}

func (r *Repository) SelectUserInfoByID(ctx context.Context, userID string) (model.User, error) {
	return r.getUserInfoByPropertyName(ctx, "Id", userID)
}

func (r *Repository) SelectUserInfoByLogin(ctx context.Context, userLogin string) ([]model.User, error) {
	var userScanner UserScanner
	var query = fmt.Sprintf(
		`SELECT
					"Id",
					"Login",
					"Email",
					"Rating",
					"PhotoUrl",
					"DisplayName"
				FROM public."User"
				WHERE "Login" = '%s'`,
		userLogin)

	err := r.Client.Query(ctx, query, &userScanner)

	if err != nil {
		return nil, xerrors.Errorf("Failed to query through Users. Error message: %w", err)
	}

	if len(userScanner.Scanned) == 0 {
		return nil, xerrors.Errorf("No user found for Login = '%s'",
			userLogin)
	}

	return userScanner.Scanned, nil
}

func (r *Repository) SelectUserFriendRequests(ctx context.Context, user model.User) ([]model.FriendRequest, error) {
	var userScanner UserScanner

	// Получаем ВХОДЯЩИЕ запросы на дружбу. Исходящие не получаем.
	var query = fmt.Sprintf(
		`SELECT 
					u."Id",
					u."Login",
					u."Email",
					u."Rating",
					u."PhotoUrl",
					u."DisplayName"
				FROM public."UserFriends" uf
				LEFT JOIN public."User" u ON uf."UserId" = u."Id"
				WHERE uf."FriendId" = '%s' AND uf."State" = '%s'`,
		user.ID, model.FriendshipPending)

	err := r.Client.Query(ctx, query, &userScanner)

	if err != nil {
		return nil, xerrors.Errorf("Failed to query through UserFriends. Error message: %w", err)
	}

	var result []model.FriendRequest

	linq.From(userScanner.Scanned).SelectT(func(user model.User) model.FriendRequest {
		return model.FriendRequest{From: user}
	}).ToSlice(&result)

	return result, nil
}

func (r *Repository) ChangeStateUserFriendRequest(ctx context.Context, user model.User, friendID string, state string) error {
	var execQuery = fmt.Sprintf(
		`UPDATE public."UserFriends"
				SET "State" = '%s'
				WHERE "FriendId" = '%s' AND "UserId" = '%s'`,
		state,
		user.ID,
		friendID)

	err := r.Client.Exec(ctx, execQuery)

	if err != nil {
		return xerrors.Errorf("Failed to execute INSERT query in UserFriend table.\nParams:\nUserID: `%s`,"+
			" FriendID: `%s`\nError message:\n%w", user.ID, friendID, err)
	}

	return nil
}

func (r *Repository) SelectUserFriends(ctx context.Context, user model.User) ([]model.User, error) {
	var userScannerByUserId UserScanner

	var queryByUserId = fmt.Sprintf(
		`SELECT
					u."Id",
					u."Login",
					u."Email",
					u."Rating",
					u."PhotoUrl",
					u."DisplayName"
				FROM public."UserFriends" f
				LEFT JOIN public."User" u ON u."Id" = f."FriendId"
				WHERE f."UserId" = '%s' AND f."State" = '%s'`, user.ID, model.FriendshipAccepted)

	var queryByFriendId = fmt.Sprintf(
		`SELECT
					u."Id",
					u."Login",
					u."Email",
					u."Rating",
					u."PhotoUrl",
					u."DisplayName"
				FROM public."UserFriends" f
				LEFT JOIN public."User" u ON u."Id" = f."UserId"
				WHERE f."FriendId" = '%s' AND f."State" = '%s'`, user.ID, model.FriendshipAccepted)

	err := r.Client.Query(ctx, queryByUserId, &userScannerByUserId)

	if err != nil {
		return nil, xerrors.Errorf("Failed to queryByUserId through UserFriends for user `%s`: %w", user.ID, err)
	}

	var userScannerByFriendId UserScanner
	err = r.Client.Query(ctx, queryByFriendId, &userScannerByFriendId)

	if err != nil {
		return nil, xerrors.Errorf("Failed to queryByFriendId through UserFriends for user `%s`: %w", user.ID, err)
	}

	mergedResult := append(userScannerByUserId.Scanned, userScannerByFriendId.Scanned...)
	var result []model.User
	linq.From(mergedResult).DistinctByT(func(user model.User) string {
		return user.ID
	}).ToSlice(&result)

	return result, nil
}

func (r *Repository) StoreUserFriend(ctx context.Context, user model.User, friendID string) error {
	var execQuery = fmt.Sprintf(
		`INSERT INTO public."UserFriends" ("UserId", "FriendId") VALUES
					('%s', '%s')`, user.ID, friendID)

	err := r.Client.Exec(ctx, execQuery)

	if err != nil {
		return xerrors.Errorf("Failed to execute INSERT query for UserFriends table.\nParams:\n"+
			"UserID: `%s`, FriendID: `%s`\nErrorMessage:\n%w", user.ID, friendID, err)
	}

	return nil
}

func (r *Repository) CreateUser(ctx context.Context, user model.User) error {
	var execQuery = fmt.Sprintf(
		`INSERT INTO public."User" VALUES
					('%s', '%s', '%s', %f, '%s', '%s')`,
		user.ID,
		user.Login,
		user.Email,
		user.Rating,
		user.PhotoURL,
		user.DisplayName)

	err := r.Client.Exec(ctx, execQuery)

	if err != nil {
		return xerrors.Errorf("Failed to execute INSERT query for User table.\nParams:\n"+
			"UserID: `%s`\nErrorMessage:\n%w", user.ID, err)
	}

	return nil
}

func (r *Repository) EditUser(ctx context.Context, user model.User) error {
	var execQuery = fmt.Sprintf(
		`UPDATE public."User"
					SET "Login" = '%s',
					"Email" = '%s',
					"Rating" = %f,
					"PhotoUrl" = '%s',
					"DisplayName" = '%s'
				WHERE "Id" = '%s'`,
		user.Login,
		user.Email,
		user.Rating,
		user.PhotoURL,
		user.DisplayName,
		user.ID)

	err := r.Client.Exec(ctx, execQuery)

	if err != nil {
		return xerrors.Errorf("Failed to execute UPDATE query for User table.\nParams:\n"+
			"UserID: `%s`\nErrorMessage:\n%w", user.ID, err)
	}

	return nil
}

func (r *Repository) selectAllUserReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	var receiptShortScanner ReceiptShortScanner

	var query = fmt.Sprintf(
		`SELECT
					r."Id",
					r."Date",
					r."Name",
					o."Id",
					o."Login",
					o."Email",
					o."Rating",
					o."PhotoUrl",
					o."DisplayName",
					d."Id",
					d."State",
					b."Id",
					b."Login",
					b."Email",
					b."Rating",
					b."PhotoUrl",
					b."DisplayName",
					deb."Id",
					deb."Login",
					deb."Email",
					deb."Rating",
					deb."PhotoUrl",
					deb."DisplayName",
					d."ReceiptId",
					d."Amount"
				FROM public."ReceiptParticipants" rp
				LEFT JOIN public."Receipt" r ON rp."ReceiptId" = r."Id"
				LEFT JOIN public."Debt" d ON d."ReceiptId" = r."Id"
				LEFT JOIN public."User" o ON o."Id" = r."OwnerId"
				LEFT JOIN public."User" b ON b."Id" = d."BorrowerId"
				LEFT JOIN public."User" deb ON deb."Id" = d."DebtorId"
				WHERE rp."UserId" = '%s'`, user.ID)

	err := r.Client.Query(ctx, query, &receiptShortScanner)

	if err != nil {
		return nil, xerrors.Errorf("Failed to query through Receipts for user `%s`: %w", user.ID, err)
	}

	var receipts []model.ReceiptShort
	linq.From(receiptShortScanner.Scanned).SelectT(func(value linq.KeyValue) model.ReceiptShort {
		return value.Value.(model.ReceiptShort)
	}).ToSlice(&receipts)

	return receipts, nil
}

func (r *Repository) getUserInfoByPropertyName(ctx context.Context, property string, propertyValue string) (model.User, error) {
	var strScanner StringScanner
	var fieldFetchQuery = fmt.Sprintf(
		`SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'User'`)

	err := r.Client.Query(ctx, fieldFetchQuery, &strScanner)

	if !linq.From(strScanner.Scanned).AnyWithT(func(fieldName string) bool {
		return strings.EqualFold(fieldName, property)
	}) {
		return model.User{}, xerrors.Errorf("Invalid property name: `%s`", property)
	}

	var userScanner UserScannerExt
	var query = fmt.Sprintf(
		`SELECT
					"Id",
					"Login",
					"Email",
					"Rating",
					"PhotoUrl",
					"DisplayName",
					"IsAdmin"
				FROM public."User"
				WHERE "%s" = '%s'`,
		property,
		propertyValue)

	err = r.Client.Query(ctx, query, &userScanner)

	if err != nil {
		return model.User{}, xerrors.Errorf("Failed to query through Users. Error message: %w", err)
	}

	if len(userScanner.Scanned) > 1 {
		return model.User{}, xerrors.Errorf("Collision occurred. More than one user with %s = '%s' found.",
			property,
			propertyValue)
	}

	if len(userScanner.Scanned) == 0 {
		return model.User{}, xerrors.Errorf("No user found for %s = '%s'",
			property,
			propertyValue)
	}

	return userScanner.Scanned[0], nil
}

func getReceiptWithGeneratedIds(receipt model.Receipt) (model.Receipt, error) {
	for i := range receipt.Items {
		itemUuid, err := uuid.NewV4()

		if err != nil {
			return model.Receipt{}, xerrors.Errorf("Generating ID for Item failed: %w", err)
		}

		receipt.Items[i].ID = itemUuid.String()
	}
	receiptUuid, err := uuid.NewV4()

	if err != nil {
		return model.Receipt{}, xerrors.Errorf("Generating ID for Receipt failed: %w", err)
	}

	receipt.ID = receiptUuid.String()

	return receipt, nil
}

func createAddDebtQuery(receipt model.Receipt, debts []model.Debt, user model.User) string {
	if len(debts) == 0 {
		return ""
	}

	var query = "INSERT INTO public.\"Debt\" (\"Id\", \"State\", \"PaidDate\", \"ReceiptId\", \"DebtorId\", \"BorrowerId\", \"Amount\") VALUES\n"
	for i := range debts {
		query += fmt.Sprintf("	('%s', '%s', %d, '%s', '%s', '%s', %g),\n",
			debts[i].ID,
			string(debts[i].State),
			debts[i].PaidDate,
			receipt.ID,
			debts[i].Debtor.ID,
			user.ID,
			debts[i].Amount)

		if i+1 == len(debts) {
			query = strings.TrimRight(query, ",\n")
		}
	}

	return query
}

func createAddParticipantQuery(receipt model.Receipt) string {
	var query = "INSERT INTO public.\"ReceiptParticipants\" (\"ReceiptId\", \"UserId\")\n" +
		fmt.Sprintf("	VALUES ('%s', '%s')", receipt.ID, receipt.Owner.ID)

	if len(receipt.Participants) > 0 {
		query += ",\n"
	}

	for i := range receipt.Participants {
		query += fmt.Sprintf("	('%s', '%s'),\n", receipt.ID, receipt.Participants[i].ID)

		if i+1 == len(receipt.Participants) {
			query = strings.TrimRight(query, ",\n")
		}
	}

	return query
}

func createItemUsersQuery(receipt model.Receipt) string {
	var query = "INSERT INTO public.\"ItemToUsers\" VALUES\n"
	for i := range receipt.Items {
		for j := range receipt.Items[i].Participants {
			query += fmt.Sprintf("	('%s', '%s'),\n",
				receipt.Items[i].ID,
				receipt.Items[i].Participants[j].ID)
		}
		if i+1 == len(receipt.Items) {
			query = strings.TrimRight(query, ",\n")
		}
	}

	return query
}

func createAddItemsQuery(receipt model.Receipt) string {
	var query = "INSERT INTO public.\"Item\" (\"Id\", \"Name\", \"ReceiptId\", \"Quantity\", \"Price\") VALUES\n"
	for i := range receipt.Items {
		query += fmt.Sprintf("	('%s', '%s', '%s', %d, %g),\n",
			receipt.Items[i].ID,
			receipt.Items[i].Name,
			receipt.ID,
			receipt.Items[i].Quantity,
			receipt.Items[i].Price)
		if i+1 == len(receipt.Items) {
			query = strings.TrimRight(query, ",\n")
		}
	}

	return query
}

func createAddReceiptQuery(receipt model.Receipt, user model.User) string {
	var query = fmt.Sprintf(
		"INSERT INTO public.\"Receipt\" (\"Id\", \"Date\", \"OwnerId\", \"Name\", \"Description\") VALUES\n ('%s', %d, '%s', '%s', '%s')",
		receipt.ID,
		receipt.Date,
		user.ID,
		receipt.Name,
		receipt.Description)

	return query
}
