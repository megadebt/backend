package db

import (
	"context"
	"time"

	"megadebt/library/logger"
	"megadebt/loadtest"

	"megadebt/model"
)

type RepositoryWithMetrics struct {
	Repository *Repository
	Logger     *logger.ContextLogger
}

func (r *RepositoryWithMetrics) Init(logger *logger.ContextLogger) {
	r.Repository = &Repository{}
	r.Repository.Client = &ClientWithMetrics{}
	r.Logger = logger
	r.Repository.Client.Init(context.Background(), logger)
}

func (r *RepositoryWithMetrics) CreateUser(ctx context.Context, user model.User) error {
	defer loadtest.MeasureTiming(time.Now(), "Create User")
	return r.Repository.CreateUser(ctx, user)
}

func (r *RepositoryWithMetrics) EditUser(ctx context.Context, user model.User) error {
	defer loadtest.MeasureTiming(time.Now(), "Edit User")
	return r.Repository.EditUser(ctx, user)
}

func (r *RepositoryWithMetrics) SelectUserReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserReceipts")
	return r.Repository.SelectUserReceipts(ctx, user)
}

func (r *RepositoryWithMetrics) SelectUserHistoryReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserHistoryReceipts")
	return r.Repository.SelectUserHistoryReceipts(ctx, user)
}

func (r *RepositoryWithMetrics) SelectUserReceiptByID(ctx context.Context, user model.User, receiptID string) (model.Receipt, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserReceiptByID")
	return r.Repository.SelectUserReceiptByID(ctx, user, receiptID)
}

func (r *RepositoryWithMetrics) SelectUserDebtByID(ctx context.Context, user model.User, debtID string) (model.Debt, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserDebtByID")
	return r.Repository.SelectUserDebtByID(ctx, user, debtID)
}

func (r *RepositoryWithMetrics) CreateUserReceipt(ctx context.Context, user model.User, receipt model.Receipt) (string, error) {
	defer loadtest.MeasureTiming(time.Now(), "CreateUserReceipt")
	return r.Repository.CreateUserReceipt(ctx, user, receipt)
}

func (r *RepositoryWithMetrics) StoreUserDebtState(ctx context.Context, user model.User, debtID string, debtState model.DebtState) error {
	defer loadtest.MeasureTiming(time.Now(), "StoreUserDebtState")
	return r.Repository.StoreUserDebtState(ctx, user, debtID, debtState)
}

func (r *RepositoryWithMetrics) CloseUserReceipt(ctx context.Context, user model.User, receiptID string) error {
	defer loadtest.MeasureTiming(time.Now(), "CloseUserReceipt")
	return r.Repository.CloseUserReceipt(ctx, user, receiptID)
}

func (r *RepositoryWithMetrics) SelectUserInfoByID(ctx context.Context, userID string) (model.User, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserInfoByID")
	return r.Repository.SelectUserInfoByID(ctx, userID)
}

func (r *RepositoryWithMetrics) SelectUserInfoByLogin(ctx context.Context, userLogin string) ([]model.User, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserInfoByLogin")
	return r.Repository.SelectUserInfoByLogin(ctx, userLogin)
}

func (r *RepositoryWithMetrics) SelectUserFriendRequests(ctx context.Context, user model.User) ([]model.FriendRequest, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserFriendRequests")
	return r.Repository.SelectUserFriendRequests(ctx, user)
}

func (r *RepositoryWithMetrics) ChangeStateUserFriendRequest(ctx context.Context, user model.User, friendID string, state string) error {
	defer loadtest.MeasureTiming(time.Now(), "ChangeStateUserFriendRequest")
	return r.Repository.ChangeStateUserFriendRequest(ctx, user, friendID, state)
}

func (r *RepositoryWithMetrics) SelectUserFriends(ctx context.Context, user model.User) ([]model.User, error) {
	defer loadtest.MeasureTiming(time.Now(), "SelectUserFriends")
	return r.Repository.SelectUserFriends(ctx, user)
}

func (r *RepositoryWithMetrics) StoreUserFriend(ctx context.Context, user model.User, friendID string) error {
	defer loadtest.MeasureTiming(time.Now(), "StoreUserFriend")
	return r.Repository.StoreUserFriend(ctx, user, friendID)
}
