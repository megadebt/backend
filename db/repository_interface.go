package db

import (
	"context"

	"megadebt/model"
)

type IRepository interface {
	// receipts
	SelectUserReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error)
	SelectUserHistoryReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error)
	SelectUserReceiptByID(ctx context.Context, user model.User, receiptID string) (model.Receipt, error)
	SelectUserDebtByID(ctx context.Context, user model.User, debtID string) (model.Debt, error)
	CreateUserReceipt(ctx context.Context, user model.User, receipt model.Receipt) (string, error)
	StoreUserDebtState(ctx context.Context, user model.User, debtID string, debtState model.DebtState) error
	CloseUserReceipt(ctx context.Context, user model.User, receiptID string) error

	// social
	SelectUserInfoByID(ctx context.Context, userID string) (model.User, error)
	SelectUserInfoByLogin(ctx context.Context, userLogin string) ([]model.User, error)
	SelectUserFriendRequests(ctx context.Context, user model.User) ([]model.FriendRequest, error)
	ChangeStateUserFriendRequest(ctx context.Context, user model.User, friendID string, state string) error
	SelectUserFriends(ctx context.Context, user model.User) ([]model.User, error)
	StoreUserFriend(ctx context.Context, user model.User, friendID string) error
	CreateUser(ctx context.Context, user model.User) error
	EditUser(ctx context.Context, user model.User) error
}

type RepositoryMock struct{}

func (rm *RepositoryMock) SelectUserReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	return []model.ReceiptShort{
		{
			ID:   "receipt-id-1",
			Date: 11111,
			Name: "Посиделки в общаге",
			Debts: []model.Debt{
				{
					ID:       "debt-id-1",
					State:    model.DebtStateOpen,
					PaidDate: 0,
					Amount:   40,
					Borrower: model.User{
						ID:    "borrower-id-1",
						Login: "borrower",
						Email: "borrower@mail.ru",
					},
					Debtor: model.User{
						ID:    "debtor-id-1",
						Login: "debtor",
						Email: "debtor@mail.ru",
					},
					ReceiptID: "receipt-id-1",
				},
			},
		},
	}, nil
}

func (rm *RepositoryMock) SelectUserHistoryReceipts(ctx context.Context, user model.User) ([]model.ReceiptShort, error) {
	return []model.ReceiptShort{
		{
			ID:   "receipt-id-1-closed",
			Date: 11111,
			Name: "Посиделки в общаге",
			Debts: []model.Debt{
				{
					ID:       "debt-id-1",
					State:    model.DebtStateClosed,
					PaidDate: 100500,
					Amount:   40,
					Borrower: model.User{
						ID:    "borrower-id-1",
						Login: "borrower",
						Email: "borrower@mail.ru",
					},
					Debtor: model.User{
						ID:    "debtor-id-1",
						Login: "debtor",
						Email: "debtor@mail.ru",
					},
					ReceiptID: "receipt-id-1-closed",
				},
			},
		},
	}, nil
}

func (rm *RepositoryMock) SelectUserDebtByID(ctx context.Context, user model.User, debtID string) (model.Debt, error) {
	return model.Debt{
		ID:       "debt-id-1",
		State:    model.DebtStateOpen,
		PaidDate: 0,
		Amount:   50,
		Borrower: model.User{
			ID:    "borrower-id-1",
			Login: "borrower",
			Email: "borrower@mail.ru",
		},
		Debtor: model.User{
			ID:    "debtor-id-1",
			Login: "debtor",
			Email: "debtor@mail.ru",
		},
		ReceiptID: "receipt-id-1",
	}, nil
}

func (rm *RepositoryMock) SelectUserReceiptByID(ctx context.Context, user model.User, receiptID string) (model.Receipt, error) {
	receiptShort := model.ReceiptShort{
		ID:   "receipt-id-1",
		Date: 11111,
		Name: "Посиделки в общаге",
		Debts: []model.Debt{
			{
				ID:       "heh",
				State:    model.DebtStateClosed,
				PaidDate: 100500,
				Amount:   40,
				Borrower: model.User{
					ID:    "borrower-id-1",
					Login: "borrower",
					Email: "borrower@mail.ru",
				},
				Debtor: model.User{
					ID:    "debtor-id-1",
					Login: "debtor",
					Email: "debtor@mail.ru",
				},
				ReceiptID: "receipt-id-1",
			},
		},
		Owner: model.User{
			ID:    "user-11111",
			Login: "norchine",
			Email: "norchinepochta@mail.ru",
		},
	}
	return model.Receipt{
		ReceiptShort: receiptShort,
		Description:  "Посидели вчетвером в общаге, пообсуждали что-нибудь",
		Items: []model.Item{
			{
				ID:       "bottle-id-1",
				Name:     "Крушовица",
				Quantity: 5,
				Price:    120,
			},
		},
	}, nil
}

func (rm *RepositoryMock) CreateUserReceipt(ctx context.Context, user model.User, receipt model.Receipt) (string, error) {
	return "receipt-id-1", nil
}

func (rm *RepositoryMock) StoreUserDebtState(ctx context.Context, user model.User, debtID string, debtState model.DebtState) error {
	return nil
}

func (rm *RepositoryMock) CloseUserReceipt(ctx context.Context, user model.User, receiptID string) error {
	return nil
}

func (rm *RepositoryMock) SelectUserFriends(ctx context.Context, user model.User) ([]model.User, error) {
	return []model.User{
		{
			ID:    "user-11111",
			Login: "norchine",
			Email: "norchinepochta@mail.ru",
		},
	}, nil
}

func (rm *RepositoryMock) StoreUserFriend(ctx context.Context, user model.User, friendID string) error {
	return nil
}

func (rm *RepositoryMock) SelectUserInfoByID(ctx context.Context, userID string) (model.User, error) {
	return model.User{
		ID:       "borrower-id-1",
		Login:    "Norchine",
		Email:    "fiction@mail.ru",
		Rating:   5,
		PhotoURL: "https://i.ya-webdesign.com/images/doge-face-png-6.png",
	}, nil
}

func (rm *RepositoryMock) SelectUserInfoByLogin(ctx context.Context, userLogin string) ([]model.User, error) {
	usr := model.User{
		ID:       "borrower-id-1",
		Login:    "Norchine",
		Email:    "fiction@mail.ru",
		Rating:   5,
		PhotoURL: "https://i.ya-webdesign.com/images/doge-face-png-6.png",
	}
	return []model.User{usr}, nil
}

func (rm *RepositoryMock) SelectUserFriendRequests(ctx context.Context, user model.User) ([]model.FriendRequest, error) {
	return []model.FriendRequest{
		{
			From: model.User{
				ID:       "debtor-id-1",
				Login:    "Record_r",
				Email:    "fiction_2@mail.ru",
				Rating:   5,
				PhotoURL: "https://i.ya-webdesign.com/images/doge-face-png-6.png",
			},
		},
	}, nil
}

func (rm *RepositoryMock) ChangeStateUserFriendRequest(ctx context.Context, user model.User, friendID string, state string) error {
	return nil
}

func (rm *RepositoryMock) CreateUser(ctx context.Context, user model.User) error {
	return nil
}

func (rm *RepositoryMock) EditUser(ctx context.Context, user model.User) error {
	return nil
}
