package db

import (
	"context"

	"github.com/jackc/pgx"
)

type IScanner interface {
	Scan(ctx context.Context, row pgx.Row) error
}
