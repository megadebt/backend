package db

import (
	"context"
	"megadebt/loadtest"

	"megadebt/model"

	"github.com/jackc/pgx"
	"golang.org/x/xerrors"
)

type ReceiptShortScanner struct {
	Scanned map[string]model.ReceiptShort
}

type StringScanner struct {
	Scanned []string
}

type DebtScanner struct {
	Scanned []model.Debt
}

type ReceiptScanner struct {
	Scanned map[string]model.Receipt
}

type ItemScanner struct {
	Scanned map[string]model.Item
}

type UserScanner struct {
	Scanned []model.User
}

type UserScannerExt struct {
	Scanned []model.User
}

func (receiptScanner *ReceiptShortScanner) Scan(ctx context.Context, row pgx.Row) error {
	var currentReceipt = model.ReceiptShort{}
	var currentDebt = model.Debt{}
	var debtState string

	err := row.Scan(&currentReceipt.ID,
		&currentReceipt.Date,
		&currentReceipt.Name,
		&currentReceipt.Owner.ID,
		&currentReceipt.Owner.Login,
		&currentReceipt.Owner.Email,
		&currentReceipt.Owner.Rating,
		&currentReceipt.Owner.PhotoURL,
		&currentReceipt.Owner.DisplayName,
		&currentDebt.ID,
		&debtState,
		&currentDebt.Borrower.ID,
		&currentDebt.Borrower.Login,
		&currentDebt.Borrower.Email,
		&currentDebt.Borrower.Rating,
		&currentDebt.Borrower.PhotoURL,
		&currentDebt.Borrower.DisplayName,
		&currentDebt.Debtor.ID,
		&currentDebt.Debtor.Login,
		&currentDebt.Debtor.Email,
		&currentDebt.Debtor.Rating,
		&currentDebt.Debtor.PhotoURL,
		&currentDebt.Debtor.DisplayName,
		&currentDebt.ReceiptID,
		&currentDebt.Amount)

	currentDebt.State = model.DebtState(debtState)

	if err != nil {
		return xerrors.Errorf("Scanning receiptShort failed: %w", err)
	}

	currentReceipt.Debts = append(currentReceipt.Debts, currentDebt)

	if receiptScanner.Scanned == nil {
		receiptScanner.Scanned = make(map[string]model.ReceiptShort)
	}

	receiptFromDict, exist := receiptScanner.Scanned[currentReceipt.ID]

	if !exist {
		receiptScanner.Scanned[currentReceipt.ID] = currentReceipt
		return nil
	}

	receiptFromDict.Debts = append(receiptFromDict.Debts, currentDebt)
	receiptScanner.Scanned[currentReceipt.ID] = receiptFromDict

	return nil
}

func (stringScanner *StringScanner) Scan(ctx context.Context, row pgx.Row) error {
	var str string

	err := row.Scan(&str)

	if err != nil {
		return xerrors.Errorf("Scanning string failed: %w", err)
	}

	stringScanner.Scanned = append(stringScanner.Scanned, str)

	return nil
}

func (debtScanner *DebtScanner) Scan(ctx context.Context, row pgx.Row) error {
	var debt model.Debt

	err := row.Scan(&debt.ID,
		&debt.State,
		&debt.Amount,
		&debt.Borrower.ID,
		&debt.Borrower.Login,
		&debt.Borrower.Email,
		&debt.Borrower.Rating,
		&debt.Borrower.PhotoURL,
		&debt.Borrower.DisplayName,
		&debt.Debtor.ID,
		&debt.Debtor.Login,
		&debt.Debtor.Email,
		&debt.Debtor.Rating,
		&debt.Debtor.PhotoURL,
		&debt.Debtor.DisplayName,
		&debt.PaidDate,
		&debt.ReceiptID)

	if err != nil {
		return xerrors.Errorf("Scanning debt failed: %w", err)
	}

	debtScanner.Scanned = append(debtScanner.Scanned, debt)

	return nil
}

func (receiptScanner *ReceiptScanner) Scan(ctx context.Context, row pgx.Row) error {
	var timer loadtest.TimeBenchmark
	var currentReceipt = model.Receipt{}
	var currentDebt = model.Debt{}
	var debtState string
	timer.Start()
	err := row.Scan(&currentReceipt.ID,
		&currentReceipt.Date,
		&currentReceipt.Name,
		&currentReceipt.Owner.ID,
		&currentReceipt.Owner.Login,
		&currentReceipt.Owner.Email,
		&currentReceipt.Owner.Rating,
		&currentReceipt.Owner.PhotoURL,
		&currentReceipt.Owner.DisplayName,
		&currentReceipt.Description,
		&currentDebt.ID,
		&debtState,
		&currentDebt.PaidDate,
		&currentDebt.Amount,
		&currentDebt.ReceiptID,
		&currentDebt.Borrower.ID,
		&currentDebt.Borrower.Login,
		&currentDebt.Borrower.Email,
		&currentDebt.Borrower.Rating,
		&currentDebt.Borrower.PhotoURL,
		&currentDebt.Borrower.DisplayName,
		&currentDebt.Debtor.ID,
		&currentDebt.Debtor.Login,
		&currentDebt.Debtor.Email,
		&currentDebt.Debtor.Rating,
		&currentDebt.Debtor.PhotoURL,
		&currentDebt.Debtor.DisplayName)
	timer.ElapsedFor("Scanning Receipt Object")

	if err != nil {
		return xerrors.Errorf("Scanning currentReceipt failed: %w", err)
	}

	currentDebt.State = model.DebtState(debtState)

	currentReceipt.Debts = append(currentReceipt.Debts, currentDebt)

	if receiptScanner.Scanned == nil {
		receiptScanner.Scanned = make(map[string]model.Receipt)
	}

	receiptFromDict, exist := receiptScanner.Scanned[currentReceipt.ID]

	if !exist {
		receiptScanner.Scanned[currentReceipt.ID] = currentReceipt
		return nil
	}

	receiptFromDict.Debts = append(receiptFromDict.Debts, currentDebt)
	receiptScanner.Scanned[currentReceipt.ID] = receiptFromDict

	return nil
}

func (itemScanner *ItemScanner) Scan(ctx context.Context, row pgx.Row) error {
	var currentParticipant model.User
	var currentItem model.Item

	err := row.Scan(&currentItem.ID,
		&currentItem.Name,
		&currentItem.Price,
		&currentItem.Quantity,
		&currentParticipant.ID,
		&currentParticipant.Login,
		&currentParticipant.Email,
		&currentParticipant.Rating,
		&currentParticipant.PhotoURL,
		&currentParticipant.DisplayName)

	if err != nil {
		return xerrors.Errorf("Scanning item failed: %w", err)
	}

	currentItem.Participants = append(currentItem.Participants, currentParticipant)

	if itemScanner.Scanned == nil {
		itemScanner.Scanned = make(map[string]model.Item)
	}

	itemFromDict, exist := itemScanner.Scanned[currentItem.ID]

	if !exist {
		itemScanner.Scanned[currentItem.ID] = currentItem
		return nil
	}

	itemFromDict.Participants = append(itemFromDict.Participants, currentParticipant)
	itemScanner.Scanned[currentItem.ID] = itemFromDict

	return nil
}

func (userScanner *UserScanner) Scan(ctx context.Context, row pgx.Row) error {
	var user model.User

	err := row.Scan(&user.ID,
		&user.Login,
		&user.Email,
		&user.Rating,
		&user.PhotoURL,
		&user.DisplayName)

	if err != nil {
		return xerrors.Errorf("Scanning user failed: %w", err)
	}

	userScanner.Scanned = append(userScanner.Scanned, user)

	return nil
}

func (userScanner *UserScannerExt) Scan(ctx context.Context, row pgx.Row) error {
	var user model.User

	err := row.Scan(&user.ID,
		&user.Login,
		&user.Email,
		&user.Rating,
		&user.PhotoURL,
		&user.DisplayName,
		&user.Admin)

	if err != nil {
		return xerrors.Errorf("Scanning user failed: %w", err)
	}

	userScanner.Scanned = append(userScanner.Scanned, user)

	return nil
}
