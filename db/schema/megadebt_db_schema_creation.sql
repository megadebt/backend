--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 12.0

-- Started on 2020-04-07 00:52:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'WIN1251';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3011 (class 1262 OID 13012)
-- Name: postgres; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';


ALTER DATABASE postgres OWNER TO postgres;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'WIN1251';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3012 (class 0 OID 0)
-- Dependencies: 3011
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 11 (class 2615 OID 16393)
-- Name: pgagent; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA pgagent;


ALTER SCHEMA pgagent OWNER TO postgres;

--
-- TOC entry 3013 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA pgagent; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA pgagent IS 'pgAgent system tables';


--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 3014 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 3 (class 3079 OID 16394)
-- Name: pgagent; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgagent WITH SCHEMA pgagent;


--
-- TOC entry 3015 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pgagent; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgagent IS 'A PostgreSQL job scheduler';


SET default_tablespace = '';

--
-- TOC entry 223 (class 1259 OID 16598)
-- Name: Debt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Debt" (
    "Id" integer NOT NULL,
    "State" character varying(100) NOT NULL,
    "PaidDate" date,
    "ReceiptId" integer NOT NULL,
    "DebtorId" integer NOT NULL,
    "BorrowerId" integer NOT NULL,
    "Amount" double precision NOT NULL
);


ALTER TABLE public."Debt" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16615)
-- Name: DebtToItems; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."DebtToItems" (
    "DebtId" integer NOT NULL,
    "ItemId" integer NOT NULL
);


ALTER TABLE public."DebtToItems" OWNER TO postgres;

--
-- TOC entry 3016 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE "DebtToItems"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public."DebtToItems" IS '| Mapping Table |
Table repsensents items related to debts';


--
-- TOC entry 222 (class 1259 OID 16596)
-- Name: Debt_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Debt_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Debt_Id_seq" OWNER TO postgres;

--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 222
-- Name: Debt_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Debt_Id_seq" OWNED BY public."Debt"."Id";


--
-- TOC entry 221 (class 1259 OID 16587)
-- Name: Item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Item" (
    "Id" integer NOT NULL,
    "Name" text NOT NULL,
    "ReceiptId" integer NOT NULL,
    "Quantity" integer NOT NULL,
    "Price" double precision NOT NULL
);


ALTER TABLE public."Item" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16620)
-- Name: ItemToUsers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ItemToUsers" (
    "ItemId" integer NOT NULL,
    "UserId" integer NOT NULL
);


ALTER TABLE public."ItemToUsers" OWNER TO postgres;

--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE "ItemToUsers"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public."ItemToUsers" IS '| Mapping Table |
Table represents users related to item';


--
-- TOC entry 220 (class 1259 OID 16585)
-- Name: Item_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Item_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Item_Id_seq" OWNER TO postgres;

--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 220
-- Name: Item_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Item_Id_seq" OWNED BY public."Item"."Id";


--
-- TOC entry 219 (class 1259 OID 16575)
-- Name: Receipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Receipt" (
    "Id" integer NOT NULL,
    "Date" date NOT NULL,
    "OwnerId" integer NOT NULL,
    "Name" text NOT NULL,
    "Description" text NOT NULL,
    "State" character varying(100) NOT NULL,
    "Amount" double precision NOT NULL
);


ALTER TABLE public."Receipt" OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16625)
-- Name: ReceiptToParticipants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ReceiptToParticipants" (
    "ReceiptId" integer NOT NULL,
    "UserId" integer NOT NULL
);


ALTER TABLE public."ReceiptToParticipants" OWNER TO postgres;

--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE "ReceiptToParticipants"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public."ReceiptToParticipants" IS '| Mapping Table |
Table represents participants of receipt';


--
-- TOC entry 218 (class 1259 OID 16573)
-- Name: Receipt_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Receipt_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Receipt_Id_seq" OWNER TO postgres;

--
-- TOC entry 3021 (class 0 OID 0)
-- Dependencies: 218
-- Name: Receipt_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Receipt_Id_seq" OWNED BY public."Receipt"."Id";


--
-- TOC entry 225 (class 1259 OID 16606)
-- Name: User; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."User" (
    "Id" integer NOT NULL,
    "Login" character varying(500) NOT NULL,
    "Mail" text
);


ALTER TABLE public."User" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16604)
-- Name: User_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."User_Id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."User_Id_seq" OWNER TO postgres;

--
-- TOC entry 3022 (class 0 OID 0)
-- Dependencies: 224
-- Name: User_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."User_Id_seq" OWNED BY public."User"."Id";


--
-- TOC entry 2834 (class 2604 OID 16601)
-- Name: Debt Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Debt" ALTER COLUMN "Id" SET DEFAULT nextval('public."Debt_Id_seq"'::regclass);


--
-- TOC entry 2833 (class 2604 OID 16590)
-- Name: Item Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Item" ALTER COLUMN "Id" SET DEFAULT nextval('public."Item_Id_seq"'::regclass);


--
-- TOC entry 2832 (class 2604 OID 16578)
-- Name: Receipt Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Receipt" ALTER COLUMN "Id" SET DEFAULT nextval('public."Receipt_Id_seq"'::regclass);


--
-- TOC entry 2835 (class 2604 OID 16609)
-- Name: User Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User" ALTER COLUMN "Id" SET DEFAULT nextval('public."User_Id_seq"'::regclass);


--
-- TOC entry 2869 (class 2606 OID 16619)
-- Name: DebtToItems DebtToItems_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DebtToItems"
    ADD CONSTRAINT "DebtToItems_pkey" PRIMARY KEY ("DebtId", "ItemId");


--
-- TOC entry 2864 (class 2606 OID 16603)
-- Name: Debt Debt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Debt"
    ADD CONSTRAINT "Debt_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2871 (class 2606 OID 16624)
-- Name: ItemToUsers ItemToUsers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ItemToUsers"
    ADD CONSTRAINT "ItemToUsers_pkey" PRIMARY KEY ("UserId", "ItemId");


--
-- TOC entry 2862 (class 2606 OID 16595)
-- Name: Item Item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Item"
    ADD CONSTRAINT "Item_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2873 (class 2606 OID 16629)
-- Name: ReceiptToParticipants ReceiptToParticipants_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ReceiptToParticipants"
    ADD CONSTRAINT "ReceiptToParticipants_pkey" PRIMARY KEY ("UserId", "ReceiptId");


--
-- TOC entry 2860 (class 2606 OID 16583)
-- Name: Receipt Receipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Receipt"
    ADD CONSTRAINT "Receipt_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2867 (class 2606 OID 16614)
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2865 (class 1259 OID 16698)
-- Name: fki_Debt_Receipt_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "fki_Debt_Receipt_fk" ON public."Debt" USING btree ("ReceiptId");


--
-- TOC entry 2879 (class 2606 OID 16650)
-- Name: DebtToItems DebtToItems_Debt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DebtToItems"
    ADD CONSTRAINT "DebtToItems_Debt" FOREIGN KEY ("DebtId") REFERENCES public."Debt"("Id") NOT VALID;


--
-- TOC entry 2880 (class 2606 OID 16655)
-- Name: DebtToItems DebtToItems_Item; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DebtToItems"
    ADD CONSTRAINT "DebtToItems_Item" FOREIGN KEY ("ItemId") REFERENCES public."Item"("Id") NOT VALID;


--
-- TOC entry 2877 (class 2606 OID 16640)
-- Name: Debt Debt_Receipt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Debt"
    ADD CONSTRAINT "Debt_Receipt_fkey" FOREIGN KEY ("ReceiptId") REFERENCES public."Receipt"("Id") NOT VALID;


--
-- TOC entry 2878 (class 2606 OID 16645)
-- Name: Debt Debt_User_Borrower_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Debt"
    ADD CONSTRAINT "Debt_User_Borrower_fkey" FOREIGN KEY ("BorrowerId") REFERENCES public."User"("Id") NOT VALID;


--
-- TOC entry 2876 (class 2606 OID 16635)
-- Name: Debt Debt_User_Debtor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Debt"
    ADD CONSTRAINT "Debt_User_Debtor_fkey" FOREIGN KEY ("DebtorId") REFERENCES public."User"("Id") NOT VALID;


--
-- TOC entry 2881 (class 2606 OID 16665)
-- Name: ItemToUsers ItemToUsers_Item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ItemToUsers"
    ADD CONSTRAINT "ItemToUsers_Item_fkey" FOREIGN KEY ("ItemId") REFERENCES public."Item"("Id") NOT VALID;


--
-- TOC entry 2882 (class 2606 OID 16670)
-- Name: ItemToUsers ItemToUsers_User_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ItemToUsers"
    ADD CONSTRAINT "ItemToUsers_User_fkey" FOREIGN KEY ("UserId") REFERENCES public."User"("Id") NOT VALID;


--
-- TOC entry 2875 (class 2606 OID 16660)
-- Name: Item Item_Receipt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Item"
    ADD CONSTRAINT "Item_Receipt_fkey" FOREIGN KEY ("ReceiptId") REFERENCES public."Receipt"("Id") NOT VALID;


--
-- TOC entry 2883 (class 2606 OID 16680)
-- Name: ReceiptToParticipants ReceiptToParticipants_Receipt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ReceiptToParticipants"
    ADD CONSTRAINT "ReceiptToParticipants_Receipt_fkey" FOREIGN KEY ("ReceiptId") REFERENCES public."Receipt"("Id") NOT VALID;


--
-- TOC entry 2884 (class 2606 OID 16685)
-- Name: ReceiptToParticipants ReceiptToParticipants_User_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ReceiptToParticipants"
    ADD CONSTRAINT "ReceiptToParticipants_User_fkey" FOREIGN KEY ("UserId") REFERENCES public."User"("Id") NOT VALID;


--
-- TOC entry 2874 (class 2606 OID 16675)
-- Name: Receipt Receipt_User_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Receipt"
    ADD CONSTRAINT "Receipt_User_fkey" FOREIGN KEY ("OwnerId") REFERENCES public."User"("Id") NOT VALID;


-- Completed on 2020-04-07 00:52:01

--
-- PostgreSQL database dump complete
--

