FROM golang:latest

RUN mkdir /app

ADD . /go/src/megadebt

WORKDIR /go/src/megadebt

ENV GOOGLE_APPLICATION_CREDENTIALS /go/src/megadebt/megadebt-credentials.json

RUN go get ./...

RUN go build -o main ./cmd/server/server.go

EXPOSE 8080

CMD ["./main"]

