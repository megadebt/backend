package mobile

import (
	"megadebt/model"
)

type DebtView struct {
	ID        string          `json:"id"`
	State     model.DebtState `json:"state"`
	PaidDate  uint64          `json:"paid_date"`
	ReceiptID string          `json:"receipt_id"`
	Borrower  UserView        `json:"borrower"`
	Debtor    UserView        `json:"debtor"`
	Amount    float64         `json:"amount"`
}

func (dv *DebtView) FromDebt(debt model.Debt) {
	dv.ID = debt.ID
	dv.State = debt.State
	dv.PaidDate = debt.PaidDate
	dv.ReceiptID = debt.ReceiptID
	dv.Borrower.FromUser(debt.Borrower)
	dv.Debtor.FromUser(debt.Debtor)
	dv.Amount = debt.Amount
}

type DebtStateChangeRequest struct {
	State model.DebtState `json:"state"`
}
