package mobile

import (
	"megadebt/model"
)

type FriendAddRequest struct {
	ID string `json:"id"`
}

type FriendRequestView struct {
	Sender UserView `json:"sender"`
}

func (frv *FriendRequestView) FromFriendRequest(fr model.FriendRequest) {
	frv.Sender.FromUser(fr.From)
}
