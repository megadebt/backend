package mobile

import (
	"megadebt/model"
)

type ItemView struct {
	ID           string     `json:"id"`
	Name         string     `json:"name"`
	Quantity     int64      `json:"quantity"`
	Price        float64    `json:"price"`
	Participants []UserView `json:"participants"`
}

func (iv *ItemView) FromItem(item model.Item) {
	iv.ID = item.ID
	iv.Name = item.Name
	iv.Price = item.Price
	iv.Quantity = item.Quantity
	iv.Participants = make([]UserView, 0, len(item.Participants))
	for _, user := range item.Participants {
		var userView UserView
		userView.FromUser(user)
		iv.Participants = append(iv.Participants, userView)
	}
}
