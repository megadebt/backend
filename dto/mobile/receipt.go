package mobile

import (
	"megadebt/model"
	"megadebt/valid"
)

type ReceiptShortView struct {
	ID         string             `json:"id"`
	Date       uint64             `json:"date"`
	Name       string             `json:"name"`
	State      model.ReceiptState `json:"state"`
	Amount     float64            `json:"amount"`
	PaidAmount float64            `json:"paid_amount"`
	Owned      bool               `json:"owned"`
}

func (rv *ReceiptShortView) FromReceipt(receipt model.ReceiptShort) {
	rv.ID = receipt.ID
	rv.Date = receipt.Date
	rv.Name = receipt.Name
	rv.PaidAmount = receipt.PaidAmount()
	rv.Amount = receipt.Amount()
	rv.State = receipt.State()
}

type ReceiptView struct {
	ID           string             `json:"id"`
	Date         uint64             `json:"date"`
	Name         string             `json:"name"`
	Description  string             `json:"description"`
	State        model.ReceiptState `json:"state"`
	Amount       float64            `json:"amount"`
	PaidAmount   float64            `json:"paid_amount"`
	Owner        UserView           `json:"owner"`
	Items        []ItemView         `json:"items"`
	Participants []UserView         `json:"participants"`
	Debts        []DebtView         `json:"debts"`
}

func (rv *ReceiptView) FromReceipt(receipt model.Receipt, userID string) {
	rv.ID = receipt.ID
	rv.Date = receipt.Date
	rv.Name = receipt.Name
	rv.Description = receipt.Description
	rv.PaidAmount = receipt.PaidAmount()
	rv.Amount = receipt.Amount()
	rv.State = receipt.State()

	rv.Items = make([]ItemView, 0, len(receipt.Items))
	for _, item := range receipt.Items {
		var itemView ItemView
		itemView.FromItem(item)
		rv.Items = append(rv.Items, itemView)
	}

	rv.Debts = make([]DebtView, 0, len(receipt.Debts))
	for _, debt := range receipt.Debts {
		var debtView DebtView
		debtView.FromDebt(debt)
		// should show only user related debts
		if debtView.Debtor.ID != userID && debtView.Borrower.ID != userID {
			continue
		}
		rv.Debts = append(rv.Debts, debtView)
	}

	rv.Participants = make([]UserView, 0, len(receipt.Participants))
	for _, user := range receipt.Participants {
		var userView UserView
		userView.FromUser(user)
		rv.Participants = append(rv.Participants, userView)
	}

	rv.Owner.FromUser(receipt.Owner)
}

type ReceiptAddRequest struct {
	Name         string               `json:"name"`
	Date         uint64               `json:"date"`
	Description  string               `json:"description"`
	Items        []ReceiptItemAddView `json:"items"`
	Participants []string             `json:"participants"`
}

func (rar *ReceiptAddRequest) Validate() error {
	var verrs valid.Errors
	// name
	if err := valid.Name(rar.Name, model.ValidationReceiptNameMinLength, model.ValidationReceiptNameMaxLength); err != nil {
		verrs = append(verrs, err)
	}
	// description
	if err := valid.Description(rar.Description, 0, model.ValidationReceiptDescriptionMaxLength); err != nil {
		verrs = append(verrs, err)
	}

	// items
	for _, item := range rar.Items {
		if err := item.Validate(); err != nil {
			verrs = append(verrs, err)
		}
	}

	if len(verrs) == 0 {
		return nil
	}
	return verrs
}

type ReceiptItemAddView struct {
	Name         string   `json:"name"`
	Quantity     int64    `json:"quantity"`
	Price        float64  `json:"price"`
	Participants []string `json:"participants"`
}

func (riav *ReceiptItemAddView) ToItem() model.Item {
	participants := make([]model.User, 0, len(riav.Participants))
	for _, id := range riav.Participants {
		participants = append(participants, model.User{ID: id})
	}
	return model.Item{
		Name:         riav.Name,
		Quantity:     riav.Quantity,
		Price:        riav.Price,
		Participants: participants,
	}
}

func (riav *ReceiptItemAddView) Validate() error {
	var verrs valid.Errors

	if err := valid.Name(riav.Name, model.ValidationItemNameMinLength, model.ValidationItemNameMaxLength); err != nil {
		verrs = append(verrs, err)
	}

	if riav.Price < 0 {
		verrs = append(verrs, &model.ErrPriceNegativeValue{})
	}

	if riav.Quantity < 0 {
		verrs = append(verrs, &model.ErrQuantityNegativeValue{})
	}

	if len(verrs) == 0 {
		return nil
	}
	return verrs
}

func (rar *ReceiptAddRequest) ToReceipt() model.Receipt {
	receiptShort := model.ReceiptShort{
		Date: rar.Date,
		Name: rar.Name,
	}

	participants := make([]model.User, 0, len(rar.Participants))
	for _, id := range rar.Participants {
		participants = append(participants, model.User{ID: id})
	}

	items := make([]model.Item, 0, len(rar.Participants))
	for _, item := range rar.Items {
		items = append(items, item.ToItem())
	}

	return model.Receipt{
		ReceiptShort: receiptShort,
		Description:  rar.Description,
		Items:        items,
		Participants: participants,
	}
}

type ReceiptAddResponse struct {
	ReceiptID string `json:"receipt_id"`
}
