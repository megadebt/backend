package mobile

import (
	"strings"
	"testing"

	"megadebt/model"

	"github.com/stretchr/testify/assert"
	"golang.org/x/xerrors"
)

func Test_ValidateReceiptAddRequest(t *testing.T) {
	type testCase struct {
		input       ReceiptAddRequest
		expectedErr model.MobileError
		name        string
	}
	tests := []testCase{
		{
			input: ReceiptAddRequest{
				Name: "Имечко",
				Items: []ReceiptItemAddView{
					{
						Name:     "Имя 2",
						Quantity: 1,
						Price:    -100,
					},
				},
			},
			expectedErr: &model.ErrPriceNegativeValue{},
			name:        "negative price",
		},
		{
			input: ReceiptAddRequest{
				Name: "Имечко",
				Items: []ReceiptItemAddView{
					{
						Name:     "",
						Quantity: 1,
						Price:    100,
					},
				},
			},
			expectedErr: &model.ErrNameMinLength{Min: model.ValidationItemNameMinLength},
			name:        "empty name",
		},
		{
			input: ReceiptAddRequest{
				Name: "супердлинноеимядляэтогособытиячтооноажнепомещаетсясюда",
				Items: []ReceiptItemAddView{
					{
						Name:     "Пиво",
						Quantity: 1,
						Price:    100,
					},
				},
			},
			expectedErr: &model.ErrNameMaxLength{Max: model.ValidationReceiptNameMaxLength},
			name:        "too long receipt name",
		},
		{
			input: ReceiptAddRequest{
				Name: "Посиделки",
				Items: []ReceiptItemAddView{
					{
						Name:     "Пиво",
						Quantity: 1,
						Price:    100,
					},
				},
				Description: strings.Repeat("a", model.ValidationReceiptDescriptionMaxLength+1),
			},
			expectedErr: &model.ErrDescriptionMaxLength{Max: model.ValidationReceiptDescriptionMaxLength},
			name:        "too long description",
		},
		{
			input: ReceiptAddRequest{
				Name: "Invalid symbols ^&",
				Items: []ReceiptItemAddView{
					{
						Name:     "Пиво",
						Quantity: 1,
						Price:    100,
					},
				},
			},
			expectedErr: &model.ErrNameCharError{},
			name:        "invalid symbols in name",
		},
		{
			input: ReceiptAddRequest{
				Name: "Кайф",
				Items: []ReceiptItemAddView{
					{
						Name:     "Пиво",
						Quantity: 1,
						Price:    100,
					},
				},
				Description: "^_^",
			},
			expectedErr: &model.ErrDescriptionCharError{},
			name:        "invalid symbols in description",
		},
		{
			input: ReceiptAddRequest{
				Name: "Тут все почти норм",
				Items: []ReceiptItemAddView{
					{
						Name:     "Пиво ^_^",
						Quantity: 1,
						Price:    100,
					},
				},
			},
			expectedErr: &model.ErrNameCharError{},
			name:        "invalid symbols in item name",
		},
		{
			input: ReceiptAddRequest{
				Name: "Тут все ок",
				Items: []ReceiptItemAddView{
					{
						Name:     "Пиво",
						Quantity: 10,
						Price:    100,
					},
				},
			},
			expectedErr: nil,
			name:        "valid struct",
		},
	}

	for _, test := range tests {
		err := test.input.Validate()
		if test.expectedErr == nil {
			assert.NoError(t, err, test.name)
			continue
		}
		var me model.MobileError
		assert.True(t, xerrors.As(err, &me), test.name)
		assert.Equal(t, test.expectedErr, me, test.name)
	}
}
