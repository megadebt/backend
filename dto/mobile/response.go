package mobile

type Response struct {
	Status       string      `json:"status"`
	RequestID    string      `json:"request_id"`
	ErrorCode    string      `json:"error_code,omitempty"`
	ErrorMessage string      `json:"error_message,omitempty"`
	Payload      interface{} `json:"payload,omitempty"`
}
