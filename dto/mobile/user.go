package mobile

import (
	"megadebt/model"
	"megadebt/valid"
)

type UserView struct {
	ID          string  `json:"id"`
	Login       string  `json:"login"`
	Mail        string  `json:"mail"`
	Rating      float64 `json:"rating"`
	PhotoURL    string  `json:"photo_url"`
	DisplayName string  `json:"display_name"`
}

func (uv *UserView) FromUser(user model.User) {
	uv.ID = user.ID
	uv.Login = user.Login
	uv.Mail = user.Email
	uv.Rating = user.Rating
	uv.PhotoURL = user.PhotoURL
	uv.DisplayName = user.DisplayName
}

type UserInfoByLoginRequest struct {
	Login string `json:"login"`
}

type UserRegisterRequest struct {
	Login string `json:"login"`
}

func (urr UserRegisterRequest) Validate() error {
	var verrs valid.Errors
	// login
	if err := valid.Login(urr.Login, model.ValidationLoginMinLength, model.ValidationLoginMaxLength); err != nil {
		verrs = append(verrs, err)
	}

	if len(verrs) == 0 {
		return nil
	}
	return verrs
}

type UserEditRequest struct {
	Login       string `json:"login"`
	DisplayName string `json:"display_name"`
}

func (urr UserEditRequest) Validate() error {
	var verrs valid.Errors
	// login
	if err := valid.Login(urr.Login, model.ValidationLoginMinLength, model.ValidationLoginMaxLength); err != nil {
		verrs = append(verrs, err)
	}

	// display name
	if err := valid.DisplayName(urr.DisplayName, model.ValidationDisplayNameMinLength, model.ValidationDisplayNameMaxLength); err != nil {
		verrs = append(verrs, err)
	}

	if len(verrs) == 0 {
		return nil
	}
	return verrs
}

type UserPrivateInfoView struct {
	UserView
	Admin bool `json:"admin"`
}

func (upiv *UserPrivateInfoView) FromUser(user model.User) {
	upiv.UserView.FromUser(user)
	upiv.Admin = user.Admin
}
