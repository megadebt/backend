package mobile

import (
	"testing"

	"megadebt/model"

	"github.com/stretchr/testify/assert"
	"golang.org/x/xerrors"
)

func Test_UserRegisterRequest(t *testing.T) {
	type testCase struct {
		input       UserRegisterRequest
		expectedErr model.MobileError
		name        string
	}
	tests := []testCase{
		{
			input:       UserRegisterRequest{Login: ""},
			expectedErr: &model.ErrLoginLengthError{Min: model.ValidationLoginMinLength, Max: model.ValidationLoginMaxLength},
			name:        "empty login",
		},
		{
			input:       UserRegisterRequest{Login: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"},
			expectedErr: &model.ErrLoginLengthError{Min: model.ValidationLoginMinLength, Max: model.ValidationLoginMaxLength},
			name:        "too long login",
		},
		{
			input:       UserRegisterRequest{Login: "invalid&*"},
			expectedErr: &model.ErrLoginCharError{},
			name:        "invalid symbols in login",
		},
		{
			input:       UserRegisterRequest{Login: "felicis31-._"},
			expectedErr: nil,
			name:        "valid login",
		},
	}

	for _, test := range tests {
		err := test.input.Validate()
		if test.expectedErr == nil {
			assert.NoError(t, err, test.name)
			continue
		}
		var me model.MobileError
		assert.True(t, xerrors.As(err, &me), test.name)
		assert.Equal(t, test.expectedErr, me, test.name)
	}
}

func Test_UserEditRequest(t *testing.T) {
	type testCase struct {
		input       UserEditRequest
		expectedErr model.MobileError
		name        string
	}
	tests := []testCase{
		{
			input: UserEditRequest{
				Login:       "",
				DisplayName: "totally valid display name",
			},
			expectedErr: &model.ErrLoginLengthError{Min: model.ValidationLoginMinLength, Max: model.ValidationLoginMaxLength},
			name:        "empty login",
		},
		{
			input: UserEditRequest{
				Login:       "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				DisplayName: "totally valid display name",
			},
			expectedErr: &model.ErrLoginLengthError{Min: model.ValidationLoginMinLength, Max: model.ValidationLoginMaxLength},
			name:        "too long login",
		},
		{
			input: UserEditRequest{
				Login:       "invalid&*",
				DisplayName: "totally valid display name",
			},
			expectedErr: &model.ErrLoginCharError{},
			name:        "invalid symbols in login",
		},
		{
			input: UserEditRequest{
				Login:       "felicis31",
				DisplayName: "",
			},
			expectedErr: &model.ErrDisplayNameLengthError{Min: model.ValidationDisplayNameMinLength, Max: model.ValidationDisplayNameMaxLength},
			name:        "empty display name",
		},
		{
			input: UserEditRequest{
				Login:       "felicis31",
				DisplayName: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			},
			expectedErr: &model.ErrDisplayNameLengthError{Min: model.ValidationDisplayNameMinLength, Max: model.ValidationDisplayNameMaxLength},
			name:        "too long display name",
		},
		{
			input: UserEditRequest{
				Login:       "felicis31",
				DisplayName: "invalid&*",
			},
			expectedErr: &model.ErrDisplayNameCharError{},
			name:        "invalid symbols in display name",
		},
		{
			input: UserEditRequest{
				Login:       "felicis31",
				DisplayName: "totally valid name",
			},
			expectedErr: nil,
			name:        "valid",
		},
	}

	for _, test := range tests {
		err := test.input.Validate()
		if test.expectedErr == nil {
			assert.NoError(t, err, test.name)
			continue
		}
		var me model.MobileError
		assert.True(t, xerrors.As(err, &me), test.name)
		assert.Equal(t, test.expectedErr, me, test.name)
	}
}
