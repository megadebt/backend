package logger

import (
	"context"

	"megadebt/library/request_id"

	"go.uber.org/zap"
)

type ContextLogger struct {
	*zap.SugaredLogger
}

func (cl ContextLogger) WithContext(ctx context.Context) ContextLogger {
	if ctx != nil {
		if requestID := request_id.GetRequestID(ctx); len(requestID) != 0 {
			cl.SugaredLogger = cl.SugaredLogger.With(zap.String("request_id", requestID))
		}
	}
	return cl
}

func InitLogging() *ContextLogger {
	var logger *zap.Logger
	logConfig := zap.NewProductionConfig()
	encoderConfig := zap.NewProductionEncoderConfig()
	logConfig.EncoderConfig = encoderConfig
	logConfig.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	logConfig.DisableCaller = true
	logConfig.OutputPaths = []string{"stdout"}
	logger, _ = logConfig.Build()
	return &ContextLogger{SugaredLogger: logger.Sugar()}
}
