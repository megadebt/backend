package recoverer

import (
	"fmt"
	"net/http"
	"os"
	"runtime/debug"
)

// inspired by https://github.com/go-chi/chi/blob/master/middleware/recoverer.go
func Recoverer() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				// TODO: log it
				if rvr := recover(); rvr != nil {
					_, _ = fmt.Fprintf(os.Stderr, "Panic: %+v\n", rvr)
					debug.PrintStack()
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
			}()
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
