package request_id

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gofrs/uuid"
)

type ctxKeyRequestID int

const requestIDKey ctxKeyRequestID = 0

func RequestID() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			requestID := r.Header.Get("X-Request-Id")
			if requestID == "" {
				u, err := uuid.NewV4()
				if err != nil {
					requestID = "request-id-generator-is-broken-plz-halp"
				} else {
					requestID = fmt.Sprint(u.String())
				}
			}
			ctx = withRequestID(ctx, requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		}
		return http.HandlerFunc(fn)
	}
}

func GetRequestID(ctx context.Context) string {
	if ctx == nil {
		return ""
	}
	if reqID, ok := ctx.Value(requestIDKey).(string); ok {
		return reqID
	}
	return ""
}

func withRequestID(ctx context.Context, requestID string) context.Context {
	return context.WithValue(ctx, requestIDKey, requestID)
}
