package tools

import (
	"strings"
)

func StandardizeSpaces(s string) string {
	return strings.Join(strings.Fields(strings.TrimSpace(s)), " ")
}

func Contains(slice []string, s string) bool {
	for _, elem := range slice {
		if s == elem {
			return true
		}
	}
	return false
}
