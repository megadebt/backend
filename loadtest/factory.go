package loadtest

import (
	"fmt"
)

func NewScenario(scenarioType ScenarioType, host string) IScenario {
	switch scenarioType {
	case ReceiptCreatingScenarioType:
		var scenario ReceiptScenario
		scenario.Init(host)
		return &scenario
	default:
		panic(fmt.Sprintf("unknown scenario type: %q", scenarioType))
	}
}
