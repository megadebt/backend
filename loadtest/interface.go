package loadtest

import (
	"time"
)

type IScenario interface {
	Init(host string)
	Do() RunScenarioResult
}

type IRunner interface {
	RunScenario(scenario IScenario, runsPerWorker int) time.Duration
	Host() string
}
