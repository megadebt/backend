package loadtest

import (
	"time"
)

type MegadebtResponse struct {
	Status       string `json:"status"`
	RequestID    string `json:"request_id"`
	ErrorCode    string `json:"error_code"`
	ErrorMessage string `json:"error_message"`
}

type RunScenarioResult struct {
	Duration time.Duration
	Err      error
}
