package loadtest

import (
	"context"
	"time"

	"megadebt/dto/mobile"
)

type ReceiptScenario struct {
	Scenario
}

func (s *ReceiptScenario) Do() RunScenarioResult {
	var duration time.Duration
	var err error

	// TODO: config readable payload
	receiptAddRequest := mobile.ReceiptAddRequest{
		Name:        "Тестовый счет",
		Date:        11111,
		Description: "Супер тестовый счет",
		Items: []mobile.ReceiptItemAddView{
			{
				Name:         "Пиво",
				Quantity:     4,
				Price:        100,
				Participants: []string{"test-user-111", "test-user-222"},
			},
			{
				Name:         "Доширак",
				Quantity:     2,
				Price:        100,
				Participants: []string{"test-user-111", "test-user-222"},
			},
		},
		Participants: []string{"test-user-111", "test-user-222"},
	}
	_, duration, err = s.createReceipt(context.Background(), receiptAddRequest)

	return RunScenarioResult{
		Duration: duration,
		Err:      err,
	}
}
