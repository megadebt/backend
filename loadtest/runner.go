package loadtest

import (
	"sync"
	"time"

	"megadebt/library/logger"

	"golang.org/x/xerrors"
)

type Runner struct {
	maxRPS int
	logger *logger.ContextLogger
	host   string
}

func (r *Runner) Init(rps int, logger *logger.ContextLogger, host string) {
	r.maxRPS = rps
	r.logger = logger
	r.host = host
}

func (r *Runner) Host() string {
	return r.host
}

func (r *Runner) RunScenario(scenario IScenario, runsPerWorker int) time.Duration {
	var wg sync.WaitGroup
	var overallDuration time.Duration
	runnerWorkers := r.maxRPS
	runsResultChannel := make(chan RunScenarioResult)
	for i := 0; i < runnerWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			defer func() {
				if rec := recover(); rec != nil {
					err := xerrors.Errorf("Panic in forcing discovery: %v", rec)
					r.logger.Warn(err)
				}
			}()
			for j := 0; j < runsPerWorker; j++ {
				runsResultChannel <- scenario.Do()
				// to make more honest RPS
				time.Sleep(time.Second)
			}
		}()
	}

	go func() {
		wg.Wait()
		close(runsResultChannel)
	}()

	for result := range runsResultChannel {
		if result.Err != nil {
			r.logger.Warnf("failed to run scenario: %v", result.Err)
			continue
		}
		overallDuration += result.Duration
	}
	r.logger.Infof("Scenario run finished. Run took %v seconds.", overallDuration.Seconds())
	return overallDuration
}
