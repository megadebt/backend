package loadtest

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"megadebt/dto/mobile"

	"golang.org/x/xerrors"
)

type ScenarioType string

type Scenario struct {
	megadebtHost string
}

func (s *Scenario) Init(host string) {
	s.megadebtHost = host
}

func (s *Scenario) simpleHttpRequest(ctx context.Context, request *http.Request) ([]byte, error) {
	switch request.Method {
	case http.MethodPost, http.MethodPut:
		request.Header.Set("Content-Type", "application/json")
	}
	resp, err := http.DefaultClient.Do(request.WithContext(ctx))
	if err != nil {
		return nil, xerrors.Errorf("unable to send request to Megadebt: %w", err)
	}
	defer func() { _ = resp.Body.Close() }()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("request to Megadebt failed: [%d] %s", resp.StatusCode, resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, xerrors.Errorf("failed to read response body: %w", err)
	}
	var mdResponse MegadebtResponse
	if err := json.Unmarshal(body, &mdResponse); err != nil {
		return nil, xerrors.Errorf("failed to parse response body: %w", err)
	}
	if mdResponse.Status != "ok" {
		return nil, fmt.Errorf("failed to send request to Megadebt: [%s] %s", mdResponse.ErrorCode, mdResponse.ErrorMessage)
	}
	return body, nil
}

func (s *Scenario) createReceipt(ctx context.Context, receipt mobile.ReceiptAddRequest) (string, time.Duration, error) {
	payloadBytes, _ := json.Marshal(receipt)
	request, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/v1.0/m/user/receipts/add", s.megadebtHost), bytes.NewReader(payloadBytes))
	if err != nil {
		return "", 0, err
	}

	start := time.Now()
	data, err := s.simpleHttpRequest(ctx, request)
	if err != nil {
		return "", 0, err
	}
	var response struct {
		Payload struct {
			ReceiptID string `json:"receipt_id"`
		} `json:"payload"`
	}
	if err := json.Unmarshal(data, &response); err != nil {
		return "", 0, err
	}

	return response.Payload.ReceiptID, time.Since(start), nil
}
