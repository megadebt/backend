package loadtest

import (
	"fmt"
	"time"
)

type TimeBenchmark struct {
	StartTime time.Time
	Elapsed   time.Duration
}

func (r *TimeBenchmark) Start() {
	r.StartTime = time.Now()
}

func (r *TimeBenchmark) ElapsedFor(funcName string) {
	fmt.Printf("Execution of `%s` took: %d ms\n", funcName, time.Since(r.StartTime).Milliseconds())
}

func MeasureTiming(start time.Time, funcName string) {
	elapsed := time.Since(start)
	fmt.Printf("Execution of `%s` took: %d ms\n", funcName, elapsed.Milliseconds())
}
