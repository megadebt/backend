package model

var KnownReceiptStates = []ReceiptState{
	ReceiptStateOpen,
	ReceiptStateClosed,
	ReceiptStateInProgress,
}

var KnownDebtStates = []string{
	string(DebtStateOpen),
	string(DebtStateClosed),
	string(DebtStatePending),
}

const (
	FriendshipPending  string = "pending"
	FriendshipAccepted string = "accepted"
	FriendshipDeclined string = "declined"
)

const (
	ReceiptStateOpen       ReceiptState = "open"
	ReceiptStateClosed     ReceiptState = "closed"
	ReceiptStateInProgress ReceiptState = "in_progress"
)

const (
	DebtStateOpen    DebtState = "open"
	DebtStateClosed  DebtState = "closed"
	DebtStatePending DebtState = "pending"
)

// receipt validation messages
const (
	NameMinLengthErrorMessage         string = "Название должно содержать как минимум %d символов."
	NameMaxLengthErrorMessage         string = "Название должно содержать не больше %d символов."
	NameCharErrorMessage              string = "Название может содержать только кириллицу или латиницу."
	DescriptionMaxLengthErrorMessage  string = "Описание должно содержать не больше %d символов."
	DescriptionCharErrorMessage       string = "Описание может содержать только кириллицу или латиницу."
	PriceNegativeValueErrorMessage    string = "Цена не может быть отрицательной."
	QuantityNegativeValueErrorMessage string = "Количество не может быть отрицательным."
	UnknownErrorMessage               string = "Произошла неизвестная ошибка. Попробуйте позже."
)

// login validation messages
const (
	LoginCharErrorMessage         string = "В логине можно использовать латинские буквы, цифры, символы тире (-), подчеркивания (_) и точки (.)"
	LoginLengthErrorMessage       string = "Логин должен состоять не менее чем из %d символов и не более чем из %d символов."
	DisplayNameCharErrorMessage   string = "В отображаемом имени можно использовать латиницу, кириллицу, цифры, символы тире (-), подчеркивания (_) и точки (.)"
	DisplayNameLengthErrorMessage string = "Отображаемое имя должно состоять не менее чем из %d символов и не более чем из %d символов."
)

const (
	ValidationReceiptNameMinLength        int = 1
	ValidationReceiptNameMaxLength        int = 30
	ValidationReceiptDescriptionMaxLength int = 140

	ValidationItemNameMinLength int = 1
	ValidationItemNameMaxLength int = 30

	ValidationLoginMinLength int = 6
	ValidationLoginMaxLength int = 15

	ValidationDisplayNameMinLength int = 6
	ValidationDisplayNameMaxLength int = 30
)
