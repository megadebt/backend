package model

type Debt struct {
	ID        string
	State     DebtState
	Borrower  User
	Debtor    User
	PaidDate  uint64
	ReceiptID string
	Amount    float64
}

type DebtState string
