package model

import (
	"fmt"
	"net/http"
)

const (
	// TODO: Add more error types
	INTERNAL_ERROR string = "INTERNAL_ERROR"
	INVALID_VALUE  string = "INVALID_VALUE"
)

type MobileError interface {
	Message() string
	HTTPStatusCode() int
	ErrorCode() string
	Error() string
}

// helpers
type withHTTPStatusCodeOK struct{}

func (w *withHTTPStatusCodeOK) HTTPStatusCode() int { return http.StatusOK }

type validationError struct{ withHTTPStatusCodeOK }

func (ve *validationError) ErrorCode() string {
	return INVALID_VALUE
}
func (ve *validationError) Error() string {
	return INVALID_VALUE
}

// actual errors

type ErrNameMinLength struct {
	validationError
	Min int
}

func (e *ErrNameMinLength) Message() string {
	return fmt.Sprintf(NameMinLengthErrorMessage, e.Min)
}

type ErrNameMaxLength struct {
	validationError
	Max int
}

func (e *ErrNameMaxLength) Message() string {
	return fmt.Sprintf(NameMaxLengthErrorMessage, e.Max)
}

type ErrNameCharError struct{ validationError }

func (e *ErrNameCharError) Message() string {
	return NameCharErrorMessage
}

type ErrDescriptionMaxLength struct {
	validationError
	Max int
}

func (e *ErrDescriptionMaxLength) Message() string {
	return fmt.Sprintf(DescriptionMaxLengthErrorMessage, e.Max)
}

type ErrDescriptionCharError struct{ validationError }

func (e *ErrDescriptionCharError) Message() string {
	return DescriptionCharErrorMessage
}

type ErrPriceNegativeValue struct{ validationError }

func (e *ErrPriceNegativeValue) Message() string {
	return PriceNegativeValueErrorMessage
}

type ErrQuantityNegativeValue struct{ validationError }

func (e *ErrQuantityNegativeValue) Message() string {
	return QuantityNegativeValueErrorMessage
}

type ErrLoginLengthError struct {
	validationError
	Min int
	Max int
}

func (e *ErrLoginLengthError) Message() string {
	return fmt.Sprintf(LoginLengthErrorMessage, e.Min, e.Max)
}

type ErrLoginCharError struct{ validationError }

func (e *ErrLoginCharError) Message() string {
	return LoginCharErrorMessage
}

type ErrDisplayNameLengthError struct {
	validationError
	Min int
	Max int
}

func (e *ErrDisplayNameLengthError) Message() string {
	return fmt.Sprintf(DisplayNameLengthErrorMessage, e.Min, e.Max)
}

type ErrDisplayNameCharError struct{ validationError }

func (e *ErrDisplayNameCharError) Message() string {
	return DisplayNameCharErrorMessage
}

type ErrUnknownValidation struct{}

func (e *ErrUnknownValidation) Message() string {
	return UnknownErrorMessage
}

func (e *ErrUnknownValidation) HTTPStatusCode() int {
	return http.StatusInternalServerError
}

func (e *ErrUnknownValidation) ErrorCode() string {
	return INTERNAL_ERROR
}

func (e *ErrUnknownValidation) Error() string {
	return INTERNAL_ERROR
}
