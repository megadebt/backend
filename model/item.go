package model

type Item struct {
	ID           string  `json:"id"`
	Name         string  `json:"name"`
	Quantity     int64   `json:"quantity"`
	Price        float64 `json:"price"`
	Participants []User  `json:"participants"`
}
