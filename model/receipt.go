package model

import (
	"github.com/gofrs/uuid"
	"golang.org/x/xerrors"
)

type Receipt struct {
	ReceiptShort
	Description  string `json:"description"`
	Items        []Item `json:"items"`
	Participants []User `json:"participants"`
}

type ReceiptShort struct {
	ID    string `json:"id"`
	Date  uint64 `json:"date"`
	Name  string `json:"name"`
	Owner User   `json:"owner"`
	Debts []Debt `json:"debts"`
}

type ReceiptState string

func (r ReceiptShort) Amount() float64 {
	var amount float64
	for _, debt := range r.Debts {
		amount += debt.Amount
	}
	return amount
}

func (r ReceiptShort) PaidAmount() float64 {
	var paidAmount float64
	for _, debt := range r.Debts {
		if debt.State == DebtStateClosed {
			paidAmount += debt.Amount
		}
	}
	return paidAmount
}

func (r ReceiptShort) State() ReceiptState {
	allClosed := true
	allOpen := true
	for _, debt := range r.Debts {
		if debt.State == DebtStateClosed {
			allOpen = false
		}
		if debt.State == DebtStateOpen {
			allClosed = false
		}
	}
	switch {
	case !allClosed && !allOpen:
		return ReceiptStateInProgress
	case allClosed:
		return ReceiptStateClosed
	case allOpen:
		return ReceiptStateOpen
	default:
		return ReceiptStateClosed
	}
}

// it requires generated ids of items and receipt
func (r Receipt) GenerateDebts() ([]Debt, error) {
	participantToDebt := make(map[string]Debt)
	for _, participant := range r.Participants {
		newID, err := uuid.NewV4()
		if err != nil {
			return nil, xerrors.New("failed to generate id for debt")
		}
		debt := Debt{
			ID:        newID.String(),
			State:     DebtStateOpen,
			Amount:    0,
			Borrower:  User{ID: r.Owner.ID},
			Debtor:    User{ID: participant.ID},
			PaidDate:  0,
			ReceiptID: r.ID,
		}
		participantToDebt[participant.ID] = debt
	}

	// populate debts with items and amount
	for _, item := range r.Items {
		for _, participant := range item.Participants {
			if participant.ID == r.Owner.ID {
				continue
			}
			debt := participantToDebt[participant.ID]
			additionToAmount := item.Price * float64(item.Quantity) / float64(len(item.Participants))
			debt.Amount += additionToAmount
			participantToDebt[participant.ID] = debt
		}
	}

	flattened := make([]Debt, 0, len(participantToDebt))
	for _, debt := range participantToDebt {
		if debt.Amount == 0 {
			continue
		}
		flattened = append(flattened, debt)
	}
	return flattened, nil
}
