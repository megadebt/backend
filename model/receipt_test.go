package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ReceiptShortGetters(t *testing.T) {
	receiptShort := ReceiptShort{
		ID:   "receipt-id-1",
		Date: 11111,
		Name: "Посиделки в общаге",
		Debts: []Debt{
			{
				ID:       "heh",
				State:    DebtStateClosed,
				PaidDate: 100500,
				Amount:   40,
			},
			{
				ID:       "ne heh",
				State:    DebtStateOpen,
				PaidDate: 0,
				Amount:   80,
			},
		},
	}

	assert.Equal(t, 120.0, receiptShort.Amount())
	assert.Equal(t, ReceiptStateInProgress, receiptShort.State())
	assert.Equal(t, 40.0, receiptShort.PaidAmount())
}

func Test_ReceiptGenerateDebts(t *testing.T) {
	// preparing data
	borrower := User{ID: "borrower-id"}
	firstUser := User{ID: "first-user"}
	secondUser := User{ID: "second-user"}
	beer := Item{
		ID:           "item-id-1",
		Name:         "Пиво",
		Quantity:     5,
		Price:        100,
		Participants: []User{firstUser, borrower},
	}
	doshirak := Item{
		ID:           "item-id-1",
		Name:         "Дошираки",
		Quantity:     3,
		Price:        50,
		Participants: []User{secondUser},
	}
	snacks := Item{
		ID:           "item-id-1",
		Name:         "Закуска",
		Quantity:     10,
		Price:        1000,
		Participants: []User{firstUser, secondUser},
	}
	receiptShort := ReceiptShort{
		ID:    "id-1",
		Date:  11111,
		Name:  "Счет",
		Owner: borrower,
	}
	receipt := Receipt{
		ReceiptShort: receiptShort,
		Description:  "какое-то описание",
		Items: []Item{
			beer,
			doshirak,
			snacks,
		},
		Participants: []User{firstUser, secondUser},
	}

	actual, err := receipt.GenerateDebts()
	assert.NoError(t, err)

	// clear ids as we randomly generate them
	expected := []Debt{
		{
			ID:        "",
			State:     DebtStateOpen,
			Amount:    5250,
			Borrower:  borrower,
			Debtor:    firstUser,
			PaidDate:  0,
			ReceiptID: "id-1",
		},
		{
			ID:        "",
			State:     DebtStateOpen,
			Amount:    5150,
			Borrower:  borrower,
			Debtor:    secondUser,
			PaidDate:  0,
			ReceiptID: "id-1",
		},
	}
	for i := range actual {
		actual[i].ID = ""
	}
	assert.ElementsMatch(t, expected, actual)
}
