package model

type User struct {
	ID          string  `json:"id"`
	Login       string  `json:"login"`
	DisplayName string  `json:"display_name"`
	Email       string  `json:"email"`
	Rating      float64 `json:"rating"`
	PhotoURL    string  `json:"photo_url"`
	Admin       bool    `json:"admin"`
}

func (u *User) DefaultRating() float64 {
	return 5.0
}

type FriendRequest struct {
	From User `json:"from"`
}
