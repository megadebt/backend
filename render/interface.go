package render

import (
	"context"
	"net/http"
)

type IRenderer interface {
	RenderJson(ctx context.Context, w http.ResponseWriter, payload interface{})
	RenderJsonError(ctx context.Context, w http.ResponseWriter, code int, payload interface{})
	RenderMobileError(ctx context.Context, w http.ResponseWriter, code int, err error)
	RenderMobileJson(ctx context.Context, w http.ResponseWriter, payload interface{})
}
