package render

import (
	"context"
	"encoding/json"
	"net/http"

	"megadebt/dto/mobile"
	"megadebt/library/request_id"
	"megadebt/model"

	"golang.org/x/xerrors"
)

type JsonRenderer struct{}

func (j *JsonRenderer) renderJson(ctx context.Context, w http.ResponseWriter, code int, payload interface{}) {
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	_, _ = w.Write(jsonPayload) // or use fmt.Fprintln(w, jsonPayload)
}

func (j *JsonRenderer) RenderJson(ctx context.Context, w http.ResponseWriter, payload interface{}) {
	j.renderJson(ctx, w, http.StatusOK, payload)
}

func (j *JsonRenderer) RenderJsonError(ctx context.Context, w http.ResponseWriter, code int, payload interface{}) {
	j.renderJson(ctx, w, code, payload)
}

func (j *JsonRenderer) RenderMobileError(ctx context.Context, w http.ResponseWriter, code int, err error) {
	httpStatusCode := code
	errMessage := ""
	errorCode := model.INTERNAL_ERROR

	var me model.MobileError
	if xerrors.As(err, &me) {
		httpStatusCode = me.HTTPStatusCode()
		errMessage = me.Message()
		errorCode = me.ErrorCode()
	}

	mobilePayload := mobile.Response{
		Status:       "error",
		RequestID:    request_id.GetRequestID(ctx),
		ErrorCode:    errorCode,
		ErrorMessage: errMessage,
	}
	j.RenderJsonError(ctx, w, httpStatusCode, mobilePayload)
}

func (j *JsonRenderer) RenderMobileJson(ctx context.Context, w http.ResponseWriter, payload interface{}) {
	mobilePayload := mobile.Response{
		Status:    "ok",
		RequestID: request_id.GetRequestID(ctx),
		Payload:   payload,
	}
	j.RenderJson(ctx, w, mobilePayload)
}
