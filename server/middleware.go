package megadebt

import (
	"net/http"

	"megadebt/auth"
	"megadebt/db"
	"megadebt/library/logger"
)

func AuthentificateWithDB(logger *logger.ContextLogger, authentificator auth.IAuthentificator, repository db.IRepository, authHeader string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			tokenCandidate := r.Header.Get(authHeader)
			userID, err := authentificator.VerifyToken(r.Context(), tokenCandidate)
			if err != nil {
				logger.WithContext(r.Context()).Warnf("failed to check user token: %v", err)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			// token valid -> getUserFromDB
			dbUser, err := repository.SelectUserInfoByID(r.Context(), userID)
			ctx := auth.ContextWithUser(r.Context(), dbUser)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
