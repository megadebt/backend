package megadebt

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"megadebt/auth"
	"megadebt/dto/mobile"
	"megadebt/model"

	"github.com/go-chi/chi"
	"golang.org/x/xerrors"
)

func (s *Server) MobileUserReceipts(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}
	s.Logger.WithContext(r.Context()).Infof("Trying to get user %s receipts", user.ID)

	receipts, err := s.DBRepository.SelectUserReceipts(r.Context(), user)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("failed to select user %s receipts: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	receiptsView := make([]mobile.ReceiptShortView, 0, len(receipts))
	for _, receipt := range receipts {
		var receiptView mobile.ReceiptShortView
		receiptView.FromReceipt(receipt)
		if receipt.Owner.ID == user.ID {
			receiptView.Owned = true
		}
		receiptsView = append(receiptsView, receiptView)
	}

	s.Render.RenderMobileJson(r.Context(), w, receiptsView)
}

func (s *Server) MobileUserHistoryReceipts(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}
	s.Logger.WithContext(r.Context()).Infof("Trying to get user %s history receipts", user.ID)

	receipts, err := s.DBRepository.SelectUserHistoryReceipts(r.Context(), user)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("failed to select user %s history receipts: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	receiptsView := make([]mobile.ReceiptShortView, 0, len(receipts))
	for _, receipt := range receipts {
		var receiptView mobile.ReceiptShortView
		receiptView.FromReceipt(receipt)
		if receipt.Owner.ID == user.ID {
			receiptView.Owned = true
		}
		receiptsView = append(receiptsView, receiptView)
	}

	s.Render.RenderMobileJson(r.Context(), w, receiptsView)
}

func (s *Server) MobileUserReceiptAdd(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}
	s.Logger.WithContext(r.Context()).Infof("Trying to save user %s receipt", user.ID)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var rar mobile.ReceiptAddRequest
	if err := json.Unmarshal(body, &rar); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}
	if err := rar.Validate(); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("ReceiptAddRequest validation failed: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	receiptID, err := s.DBRepository.CreateUserReceipt(r.Context(), user, rar.ToReceipt())
	if err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to create user %s receipt: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	response := mobile.ReceiptAddResponse{ReceiptID: receiptID}
	s.Render.RenderMobileJson(r.Context(), w, response)
}

func (s *Server) MobileUserFriends(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}
	s.Logger.WithContext(r.Context()).Infof("Trying to get user %s friends", user.ID)

	friends, err := s.DBRepository.SelectUserFriends(r.Context(), user)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("failed to select user %s friends: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, xerrors.Errorf("failed to get user %s friends: %w", err))
		return
	}

	friendsView := make([]mobile.UserView, 0, len(friends))
	for _, friend := range friends {
		var friendView mobile.UserView
		friendView.FromUser(friend)
		friendsView = append(friendsView, friendView)
	}

	s.Render.RenderMobileJson(r.Context(), w, friendsView)
}

func (s *Server) MobileUserFriendAdd(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}
	s.Logger.WithContext(r.Context()).Infof("Trying to store user %s friend", user.ID)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var far mobile.FriendAddRequest
	if err := json.Unmarshal(body, &far); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	if err := s.DBRepository.StoreUserFriend(r.Context(), user, far.ID); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("failed to store user %s friend %s: %v", user.ID, far.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserReceiptByID(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	receiptID := chi.URLParam(r, "receipt_id")
	s.Logger.WithContext(r.Context()).Infof("Trying to get user %s receipt %s", user.ID, receiptID)

	receipt, err := s.DBRepository.SelectUserReceiptByID(r.Context(), user, receiptID)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("failed to select user %s receipt %s: %v", user.ID, receiptID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	var receiptView mobile.ReceiptView
	receiptView.FromReceipt(receipt, user.ID)
	s.Render.RenderMobileJson(r.Context(), w, receiptView)
}

func (s *Server) MobileUserReceiptAddValidation(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}
	s.Logger.WithContext(r.Context()).Infof("Trying to validate user %s receipt add request", user.ID)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var rar mobile.ReceiptAddRequest
	if err := json.Unmarshal(body, &rar); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}
	if err := rar.Validate(); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("ReceiptAddValidationRequest failed: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserReceiptClose(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	receiptID := chi.URLParam(r, "receipt_id")
	s.Logger.WithContext(r.Context()).Infof("Trying to close receipt %s by user %s", receiptID, user.ID)

	receipt, err := s.DBRepository.SelectUserReceiptByID(r.Context(), user, receiptID)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to select receipt %s for user %s: %v", receiptID, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	if receipt.Owner.ID != user.ID {
		s.Logger.WithContext(r.Context()).Errorf("Failed to close receipt %s for user %s: this receipt is not owned by this user", receiptID, user.ID)
		s.Render.RenderMobileError(r.Context(), w, http.StatusForbidden, xerrors.Errorf("failed to close receipt: forbidden"))
		return
	}
	if err := s.DBRepository.CloseUserReceipt(r.Context(), user, receiptID); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to close receipt %s for user %s: %v", receiptID, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, xerrors.Errorf("failed to close receipt %s", receiptID))
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserDebtStateChange(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	debtID := chi.URLParam(r, "debt_id")
	s.Logger.WithContext(r.Context()).Infof("Trying to change debt state %s by user %s", debtID, user.ID)

	debt, err := s.DBRepository.SelectUserDebtByID(r.Context(), user, debtID)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to select debt %s for user %s: %v", debtID, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	// do not overwrite already closed debt
	if debt.State == model.DebtStateClosed {
		s.Render.RenderMobileJson(r.Context(), w, nil)
		return
	}
	var newState model.DebtState
	switch {
	case debt.Debtor.ID == user.ID:
		newState = model.DebtStatePending
	case debt.Borrower.ID == user.ID:
		newState = model.DebtStateClosed
	default:
		s.Logger.WithContext(r.Context()).Errorf("Failed to change state of debt %s for user %s: debt is not related to user", debtID, user.ID)
		s.Render.RenderMobileError(r.Context(), w, http.StatusForbidden, xerrors.Errorf("failed to change state of debt %s", debtID))
		return
	}

	if err := s.DBRepository.StoreUserDebtState(r.Context(), user, debtID, newState); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to change state of debt %s for user %s: %v", debtID, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, xerrors.Errorf("failed to change state of debt %s", debtID))
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserFriendRequestApprove(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	friendID := chi.URLParam(r, "friend_id")
	s.Logger.WithContext(r.Context()).Infof("Trying to approve friend request for friendID %s by user %s", friendID, user.ID)
	if err := s.DBRepository.ChangeStateUserFriendRequest(r.Context(), user, friendID, model.FriendshipAccepted); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to approve friend request for friendID %s by user %s: %v", friendID, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserFriendRequestDecline(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	friendID := chi.URLParam(r, "friend_id")
	s.Logger.WithContext(r.Context()).Infof("Trying to decline friend request for friendID %s by user %s", friendID, user.ID)
	if err := s.DBRepository.ChangeStateUserFriendRequest(r.Context(), user, friendID, model.FriendshipDeclined); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to decline friend request for friendID %s by user %s: %v", friendID, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserFriendRequests(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	s.Logger.WithContext(r.Context()).Infof("Trying to get friend requests for user %s", user.ID)
	friendRequests, err := s.DBRepository.SelectUserFriendRequests(r.Context(), user)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to get friend requests for user %s: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	frViews := make([]mobile.FriendRequestView, 0, len(friendRequests))
	for _, friendRequest := range friendRequests {
		var friendRequestView mobile.FriendRequestView
		friendRequestView.FromFriendRequest(friendRequest)
		frViews = append(frViews, friendRequestView)
	}
	s.Render.RenderMobileJson(r.Context(), w, frViews)
}

func (s *Server) MobileUserInfo(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	s.Logger.WithContext(r.Context()).Infof("Trying to get user info for user %s", user.ID)
	userInfo, err := s.DBRepository.SelectUserInfoByID(r.Context(), user.ID)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to get user info for user %s: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	var userInfoView mobile.UserPrivateInfoView
	userInfoView.FromUser(userInfo)
	s.Render.RenderMobileJson(r.Context(), w, userInfoView)
}

func (s *Server) MobileUserInfoByID(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	desiredUserID := chi.URLParam(r, "userId")

	s.Logger.WithContext(r.Context()).Infof("Trying to get user info of %s for user %s", user.ID)
	userInfo, err := s.DBRepository.SelectUserInfoByID(r.Context(), desiredUserID)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to get user info for user %s: %v", desiredUserID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	var userInfoView mobile.UserView
	userInfoView.FromUser(userInfo)
	s.Render.RenderMobileJson(r.Context(), w, userInfoView)
}

func (s *Server) MobileUserInfoByLogin(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var uilr mobile.UserInfoByLoginRequest
	if err := json.Unmarshal(body, &uilr); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	s.Logger.WithContext(r.Context()).Infof("Trying to get user info by login %s for user %s", user.ID)
	userInfos, err := s.DBRepository.SelectUserInfoByLogin(r.Context(), uilr.Login)
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to get user info by login %s for user %s: %v", uilr.Login, user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	userInfoViews := make([]mobile.UserView, 0, len(userInfos))
	for _, userInfo := range userInfos {
		var userInfoView mobile.UserView
		userInfoView.FromUser(userInfo)
		userInfoViews = append(userInfoViews, userInfoView)
	}
	s.Render.RenderMobileJson(r.Context(), w, userInfoViews)
}

func (s *Server) MobileUserRegister(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var urr mobile.UserRegisterRequest
	if err := json.Unmarshal(body, &urr); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}
	if err := urr.Validate(); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("UserRegisterRequest validation failed: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	user.Rating = user.DefaultRating()
	user.Login = urr.Login

	s.Logger.WithContext(r.Context()).Infof("trying to create user %s in database", user.ID)
	if err := s.DBRepository.CreateUser(r.Context(), user); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to create user %s in database: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserRegisterValidation(w http.ResponseWriter, r *http.Request) {
	_, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var urr mobile.UserRegisterRequest
	if err := json.Unmarshal(body, &urr); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}
	if err := urr.Validate(); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("UserRegisterRequest validation failed: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}

func (s *Server) MobileUserEdit(w http.ResponseWriter, r *http.Request) {
	user, err := auth.GetUserFromContext(r.Context())
	if err != nil {
		s.Logger.WithContext(r.Context()).Errorf("User is unauthorized")
		s.Render.RenderMobileError(r.Context(), w, http.StatusUnauthorized, xerrors.Errorf("User is unauthorized"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil || len(body) == 0 {
		s.Logger.WithContext(r.Context()).Warnf("Error reading body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}

	var uer mobile.UserEditRequest
	if err := json.Unmarshal(body, &uer); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("Failed to unmarshal body: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusBadRequest, err)
		return
	}
	if err := uer.Validate(); err != nil {
		s.Logger.WithContext(r.Context()).Warnf("UserEditRequest validation failed: %v", err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}

	user.Login = uer.Login
	user.DisplayName = uer.DisplayName

	s.Logger.WithContext(r.Context()).Infof("trying to edit user %s in database", user.ID)
	if err := s.DBRepository.EditUser(r.Context(), user); err != nil {
		s.Logger.WithContext(r.Context()).Errorf("Failed to edit user %s in database: %v", user.ID, err)
		s.Render.RenderMobileError(r.Context(), w, http.StatusInternalServerError, err)
		return
	}
	s.Render.RenderMobileJson(r.Context(), w, nil)
}
