package megadebt

import (
	"context"
	"net/http"

	"megadebt/auth"
	"megadebt/db"
	"megadebt/library/logger"
	"megadebt/library/recoverer"
	"megadebt/render"

	"megadebt/library/request_id"

	"github.com/go-chi/chi"
)

type Server struct {
	Router          *chi.Mux
	Render          render.IRenderer
	Logger          *logger.ContextLogger
	Authentificator auth.IAuthentificator
	DBRepository    db.IRepository
}

func (s *Server) Init() {
	s.InitRenderer()
	s.InitLogger()
	s.InitAuthentificator()
	s.InitDBRepository()
	s.InitRouter()

	s.Logger.Infof("Megadebt server initialization complete")
}

func (s *Server) InitRenderer() {
	s.Render = &render.JsonRenderer{}
}

func (s *Server) InitLogger() {
	s.Logger = logger.InitLogging()
}

func (s *Server) InitDBRepository() {
	// mock repository used for local development
	// actual repository for deploying
	DBRepository := &db.RepositoryWithMetrics{}
	DBRepository.Init(s.Logger)
	s.DBRepository = DBRepository

	/*s.DBRepository = &db.RepositoryMock{}*/
}

func (s *Server) InitAuthentificator() {
	// TODO: use actual authentificator, not mock
	//s.Authentificator = &auth.Mock{}
	// firebase authentificator needs credentials to work, ask @norchine for it
	firebaseAuth := &auth.FirebaseAuthentificator{}
	firebaseAuth.Init(context.Background(), "")
	s.Authentificator = firebaseAuth
}

func (s *Server) InitRouter() {
	router := chi.NewRouter()

	router.Use(
		recoverer.Recoverer(),
		request_id.RequestID(),
	)

	s.Router = router

	router.HandleFunc("/ping", s.PingServer)
	router.Route("/v1.0", func(r chi.Router) {
		r.With(auth.Authentificate(s.Logger, s.Authentificator, UserTokenHeader)).
			Post("/m/user/register", s.MobileUserRegister)
		r.With(auth.Authentificate(s.Logger, s.Authentificator, UserTokenHeader)).
			Post("/m/user/register/validation", s.MobileUserRegisterValidation)
		r.Route("/m", func(r chi.Router) {
			r.Use(AuthentificateWithDB(s.Logger, s.Authentificator, s.DBRepository, UserTokenHeader))
			r.Route("/user", func(r chi.Router) {
				r.Route("/receipts", func(r chi.Router) {
					r.Get("/", s.MobileUserReceipts)
					r.Get("/history", s.MobileUserHistoryReceipts)
					r.Route("/add", func(r chi.Router) {
						r.Post("/validation", s.MobileUserReceiptAddValidation)
						r.Post("/", s.MobileUserReceiptAdd)
					})
					r.Route("/{receipt_id}", func(r chi.Router) {
						r.Get("/", s.MobileUserReceiptByID)
						r.Post("/close", s.MobileUserReceiptClose)
					})
				})
				r.Route("/debts", func(r chi.Router) {
					r.Put("/{debt_id}/state", s.MobileUserDebtStateChange)
				})
			})
			r.Route("/social", func(r chi.Router) {
				r.Route("/friends", func(r chi.Router) {
					r.Get("/", s.MobileUserFriends)
					r.Post("/add", s.MobileUserFriendAdd)
					r.Route("/requests", func(r chi.Router) {
						r.Get("/", s.MobileUserFriendRequests)
						r.Post("/{friend_id}/approve", s.MobileUserFriendRequestApprove)
						r.Post("/{friend_id}/decline", s.MobileUserFriendRequestDecline)
					})
				})
				r.Route("/info", func(r chi.Router) {
					r.Get("/", s.MobileUserInfo)
					r.Get("/about/{userId}", s.MobileUserInfoByID)
					r.Post("/edit", s.MobileUserEdit)
					r.Post("/login", s.MobileUserInfoByLogin)
				})
			})
		})
	})
}

func (s *Server) PingServer(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte("Ok"))
}
