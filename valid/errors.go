package valid

import (
	"strings"

	"golang.org/x/xerrors"
)

type Errors []error

func (es Errors) Error() string {
	glue := "; "
	result := ""
	for _, e := range es {
		result = strings.Join([]string{result, e.Error()}, glue)
	}
	return result
}

func (es Errors) Is(target error) bool {
	for _, e := range es {
		if xerrors.Is(e, target) {
			return true
		}
	}
	return false
}

func (es Errors) As(target interface{}) bool {
	for _, e := range es {
		if xerrors.As(e, target) {
			return true
		}
	}
	return false
}
