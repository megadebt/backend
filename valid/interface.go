package valid

type Validation interface {
	Validate() error
}
