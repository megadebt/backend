package valid

import (
	"regexp"
	"strings"

	"megadebt/library/tools"
	"megadebt/model"
)

var allowedSymbols = regexp.MustCompile(`^(([а-яёА-ЯЁ\-,!?_]+)|(\d+)|([a-zA-Z0-9\-,!?_]+))$`)
var loginAllowedSymbols = regexp.MustCompile(`^[a-zA-Z0-9\-._]+$`)
var displayNameAllowedSymbols = regexp.MustCompile(`^[a-zA-Zа-яёА-ЯЁ0-9\-._]+$`)

func isAllowedSymbols(s string) bool {
	stStr := tools.StandardizeSpaces(s)
	words := strings.Split(stStr, " ")
	for _, word := range words {
		if len(word) > 0 && !allowedSymbols.MatchString(word) {
			return false
		}
	}
	return true
}

func isAllowedInLoginSymbols(s string) bool {
	return loginAllowedSymbols.MatchString(s)
}

func isAllowedInDisplayNameSymbols(s string) bool {
	stStr := tools.StandardizeSpaces(s)
	words := strings.Split(stStr, " ")
	for _, word := range words {
		if len(word) > 0 && !displayNameAllowedSymbols.MatchString(word) {
			return false
		}
	}
	return true
}

func validNameLength(s string, minLimit int, maxLimit int) error {
	stStr := tools.StandardizeSpaces(s)
	switch {
	case len([]rune(stStr)) > maxLimit:
		return &model.ErrNameMaxLength{Max: maxLimit}
	case len([]rune(stStr)) < minLimit:
		return &model.ErrNameMinLength{Min: minLimit}
	}
	return nil
}

func validDescriptionLength(s string, minLimit int, maxLimit int) error {
	switch {
	case len([]rune(s)) > maxLimit:
		return &model.ErrDescriptionMaxLength{Max: maxLimit}
	}
	return nil
}

func validLoginLength(s string, minLimit int, maxLimit int) error {
	switch {
	case len([]rune(s)) > maxLimit || len([]rune(s)) < minLimit:
		return &model.ErrLoginLengthError{Min: minLimit, Max: maxLimit}
	}
	return nil
}

func validDisplayNameLength(s string, minLimit int, maxLimit int) error {
	switch {
	case len([]rune(s)) > maxLimit || len([]rune(s)) < minLimit:
		return &model.ErrDisplayNameLengthError{Min: minLimit, Max: maxLimit}
	}
	return nil
}

func Name(s string, minLimit int, maxLimit int) error {
	if err := validNameLength(s, minLimit, maxLimit); err != nil {
		return err
	}
	if !isAllowedSymbols(s) {
		return &model.ErrNameCharError{}
	}
	return nil
}

func Description(s string, minLimit int, maxLimit int) error {
	if err := validDescriptionLength(s, minLimit, maxLimit); err != nil {
		return err
	}
	if !isAllowedSymbols(s) {
		return &model.ErrDescriptionCharError{}
	}
	return nil
}

func Login(s string, minLimit int, maxLimit int) error {
	if err := validLoginLength(s, minLimit, maxLimit); err != nil {
		return err
	}
	if !isAllowedInLoginSymbols(s) {
		return &model.ErrLoginCharError{}
	}
	return nil
}

func DisplayName(s string, minLimit int, maxLimit int) error {
	if err := validDisplayNameLength(s, minLimit, maxLimit); err != nil {
		return err
	}
	if !isAllowedInDisplayNameSymbols(s) {
		return &model.ErrDisplayNameCharError{}
	}
	return nil
}
